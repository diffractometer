#include <iostream>
#include "AxisAttrib.h"

namespace Diffractometer_ns
{

	AxisAttrib::AxisAttrib(char const *name, AxisAdapter & adapter):
		Tango::Attr(name, Tango::DEV_DOUBLE, Tango::READ_WRITE),
		_adapter(adapter)
	{
		Tango::UserDefaultAttrProp axis_prop;
		axis_prop.set_label(name);
		axis_prop.set_format("%7.4f");

		std::string description;
		description += "The ";
		description += name;
		description += " axis of the diffractometer";
		axis_prop.set_description(description.c_str());
		axis_prop.set_unit("°");
		this->set_default_properties(axis_prop);
	}

	void AxisAttrib::read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{
		double read = _adapter.get_read();
		double write = _adapter.get_write();

		att.set_value(&read);
		Tango::WAttribute & watt = dev->get_device_attr()->get_w_attr_by_name(name.c_str());
		watt.set_write_value(write);
	}

	void AxisAttrib::write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
	{
		double value;

		att.get_write_value(value);
		_adapter.write(value);
	}

	bool AxisAttrib::is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{
		if (dev->get_state() == Tango::FAULT ||
				dev->get_state() == Tango::ALARM)
			if (type == Tango::READ_REQ)
				return true;
			else
				return false;
		return true;
	}

} //- end namespace
