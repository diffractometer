#ifndef __PSEUDOAXIS_ADAPTER_H__
#define __PSEUDOAXIS_ADAPTER_H__

#include <tango.h>

#include <hkl/hkl-geometry-factory.h>
#include <hkl/hkl-pseudoaxis-factory.h>

#include "AxisAdapter.h"
#include "Matrix.h"

namespace Diffractometer_ns {

	//forwar declaration
	class TangoHKLAdapter;

	struct PseudoAxisConfig{
		Tango::DevDouble position_r;
		Tango::DevDouble position_w;
		Tango::DevDouble offset;
		Tango::DevBoolean initialized;
		Tango::DevState state;
		std::string status;
		Tango::DevString mode;
		Matrix<char *> mode_names;
	};

	class PseudoAxisAdapter {
		friend class TangoHKLAdapter;

	public:
		PseudoAxisAdapter(TangoHKLAdapter & hklAdapter, HklPseudoAxis *pseudo_read, HklPseudoAxis *pseudo_write);
		virtual ~PseudoAxisAdapter(void);

		PseudoAxisConfig get_config(void);

		bool is_ready(void);

		void set_position(const Tango::DevDouble & position);
		void set_offset(const Tango::DevDouble & offset);
		void set_mode(Tango::DevString const & mode);
		void set_initialized(Tango::DevBoolean initialized);

		void get_mode_parameters(Tango::DevVarDoubleStringArray	*argout);
		void set_mode_parameters(const Tango::DevVarDoubleStringArray *argin);
		Tango::DevDouble compute_new_offset(const Tango::DevDouble & position);
		void on(void);
		void stop(void);
	private:
		TangoHKLAdapter &_hklAdapter;
		std::string _devicename;
		HklPseudoAxis *_pseudo_r;
		HklPseudoAxis *_pseudo_w;
		PseudoAxisConfig _config;
		std::vector<AxisAdapter *> _axes;
		bool _ready;

		void update(void);
		void to_proxies(void);
	};
}


#endif // __PSEUDOAXIS_ADAPTER_H__
