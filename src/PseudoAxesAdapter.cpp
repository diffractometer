#include "TangoHKLAdapter.h"
#include "PseudoAxes.h"
#include "PseudoAxesAttrib.h"
#include "PseudoAxesAdapter.h"
#include "Diffractometer.h"

namespace Diffractometer_ns
{

	PseudoAxesAdapter::PseudoAxesAdapter(TangoHKLAdapter *hklAdapter, size_t idx,
					     HklPseudoAxisEngine *engine_r,
					     HklPseudoAxisEngine *engine_w):
		_hklAdapter(hklAdapter),
		_engine_r(engine_r),
		_engine_w(engine_w),
		_idx(idx)
	{
		size_t i;
		size_t len;

		len = HKL_LIST_LEN(_engine_r->pseudoAxes);
		_config.read.resize(len, 1);
		_config.write.resize(len, 1);

		// update the _config.mode_names only once.
		len = HKL_LIST_LEN(_engine_r->modes);
		_config.mode_names.resize(len, 1);
		for(i=0; i<len; ++i)
			_config.mode_names.data[i] = const_cast<char *>(_engine_r->modes[i]->name);

		/*
		 * update the _config.pseudo_axes_names and create the dynamic attributes
		 * only once per PseudoAxesAdapter.
		 */
		len = HKL_LIST_LEN(_engine_r->pseudoAxes);
		_config.pseudo_axes_names.resize(len , 1);
		for(i=0; i<len; ++i){
			Tango::Attr *att;
			const char *name;

			name = ((HklParameter *)(_engine_r->pseudoAxes[i]))->name;
			_config.pseudo_axes_names.data[i] = const_cast<char *>(name);
			att = new PseudoAxesAxisAttrib(name, *this, i);
			_dynamic_attribute_pseudo_axes_axis.push_back(att);
		}

		// compute the proxy name
		_proxy_name = _hklAdapter->get_device()->get_name();
		_proxy_name += "-sim-";
		_proxy_name += _engine_r->name;

		this->update_axes_i(_engine_r);

		this->update();

		_device = NULL;
		_synchronize = true;
	}

	PseudoAxesAdapter::~PseudoAxesAdapter(void)
	{
		size_t i;

		for(i=0; i<_dynamic_attribute_pseudo_axes_axis.size(); ++i)
			delete _dynamic_attribute_pseudo_axes_axis[i];
	}

	PseudoAxesConfig PseudoAxesAdapter::get_config(void)
	{
		return _hklAdapter->pseudo_axes_get_config(_idx);
	}

	char const * PseudoAxesAdapter::get_name(void) const
	{
		return _hklAdapter->diffractometer()->engines_r->engines[_idx]->name;
	}

	void PseudoAxesAdapter::set_axis_value(size_t idx, double value)
	{
		_hklAdapter->pseudo_axes_set_axis_value(_idx, idx, value);
	}

	void PseudoAxesAdapter::apply(void)
	{
		_hklAdapter->pseudo_axes_apply(_idx, _config.write);
	}

	void PseudoAxesAdapter::set_mode(const Tango::DevString name)
	{
		_hklAdapter->pseudo_axes_set_mode(_idx, name);
	}

	void PseudoAxesAdapter::set_initialized(const Tango::DevBoolean initialized)
	{
		_hklAdapter->pseudo_axes_set_initialized(_idx, initialized);
	}

	void PseudoAxesAdapter::set_parameters(const Matrix<double> values)
	{
		_hklAdapter->pseudo_axes_set_mode_parameters(_idx, values);	
	}

	void PseudoAxesAdapter::init(void)
	{
		_hklAdapter->pseudo_axes_init(_idx);
	}

	void PseudoAxesAdapter::add_dynamic_attributes(void)
	{
		/*
		 * need to call the _hklAdapter method to add all
		 * the dynamic attributes of all PseudoAxesAdapter in a row.
		 * This is due to the dynamic attribute Tango behaviour.
		 * Once a device was instanciate, the next device contain already
		 * the added dynamic attributes. So you need to add the dynamic attributes
		 * once all devices were instanciate.
		 *
		 * add_dynamic_attribute_i do the real job for one PseudoAxesAdapter.
		 */
		_hklAdapter->pseudo_axes_add_dynamic_attributes(_idx);
	}

	void PseudoAxesAdapter::remove_dynamic_attributes(void)
	{
		_hklAdapter->pseudo_axes_remove_dynamic_attributes(_idx);
	}

	void PseudoAxesAdapter::load(void)
	{
		this->load_1();
	}

	/******************/
	/* private method */
	/******************/

	void PseudoAxesAdapter::update_axes_i(HklPseudoAxisEngine *engine)
	{
		size_t i, j;
		size_t len;
		char const **names;

		// fill the _axes with the right AxisAdapters
		std::vector<AxisAdapter> & axes = _hklAdapter->get_axes();
		len = HKL_LIST_LEN(engine->mode->axes_names);
		names = engine->mode->axes_names;
		_axes.clear();
		for(i=0; i<len; ++i)
			for(j=0; j<axes.size(); ++j){
				AxisAdapter & axis = axes[j];
				if(axis.get_name() == names[i]) {
					_axes.push_back(&axis);
					continue;
				}
			}
	}

	void PseudoAxesAdapter::add_dynamic_attributes_i(void)
	{
		if(_device){
			size_t i;
			Tango::MultiAttribute *multiattribute = _device->get_device_attr();

			for(i=0; i<_dynamic_attribute_pseudo_axes_axis.size(); ++i){
				try{
					multiattribute->get_attr_by_name(_config.pseudo_axes_names.data[i]);
				}catch (Tango::DevFailed){
					_device->add_attribute(_dynamic_attribute_pseudo_axes_axis[i]);
				}
			}
		}
	}

	void PseudoAxesAdapter::update(void)
	{
		size_t i, len;
		std::string status_extra;

		// update the read/write part.
		len = HKL_LIST_LEN(_engine_r->pseudoAxes);
		for(i=0; i<len; ++i){
			_config.read.data[i] = hkl_parameter_get_value_unit((HklParameter *)_engine_r->pseudoAxes[i]);
			if(_synchronize)
				_config.write.data[i] = hkl_parameter_get_value_unit((HklParameter *)_engine_w->pseudoAxes[i]);
		}

		// update the state and status.
		_config.status = "PseudoAxes status: ";

		if(!_hklAdapter->_auto_update_from_proxies)
			_config.state = Tango::STANDBY;
		else{
			_config.state = Tango::STANDBY;
			for(i=0;i<_axes.size();++i) {
				AxisAdapter const * axis = _axes[i];
				::compose_state(_config.state, axis->get_state());
				if (axis->get_state() != Tango::STANDBY)
					status_extra += "\n" + axis->get_proxy_name() + " is in " + Tango::DevStateName[axis->get_state()];
			}
		}
		_config.status += Tango::DevStateName[_config.state];
		_config.status += status_extra;

		// update the initialized
		if(_engine_r->mode->initialize == NULL)
			_config.initialized = true;

		// update the mode
		if(_engine_r->mode){
			if(_config.mode != _engine_r->mode->name){
				size_t i;
				size_t len;

				_config.mode = const_cast<Tango::DevString>(_engine_r->mode->name);
				len = HKL_LIST_LEN(_engine_r->mode->parameters);
				_config.parameters.resize(len, 1);
				_config.parameters_names.resize(len, 1);
				for(i=0; i<len; ++i){
					_config.parameters.data[i] = hkl_parameter_get_value_unit(&_engine_r->mode->parameters[i]);
					_config.parameters_names.data[i] = const_cast<char *>(_engine_r->mode->parameters[i].name);
				}
			}
		}else
			_config.mode = NULL;
	}

	void PseudoAxesAdapter::save(void)
	{
		Tango::DbData mode_prop;
		Tango::DbData data_put;
		HklPseudoAxisEngine *engine;
		size_t i, j, len;

		// first erase the old mode properties
		mode_prop.push_back(Tango::DbDatum("Mode"));
		_device->get_db_device()->get_attribute_property(mode_prop);
		long number_of_prop = 0;
		mode_prop[0] >> number_of_prop ;
		if(number_of_prop > 0)
			_device->get_db_device()->delete_attribute_property(mode_prop);

		// Step 2 : create the Mode properties
		engine = _hklAdapter->diffractometer()->engines_r->engines[_idx];
		Tango::DbDatum properties("Mode");
		// Put number of properties (= nb of samples + 1)
		len = HKL_LIST_LEN(engine->modes);
		properties << (long)(len);
		data_put.push_back(properties);

		for(i=0; i<len; ++i){
			HklPseudoAxisEngineMode *mode;
			std::vector<std::string> lines;
			char line[256];

			// the mode
			mode = engine->modes[i];
			for(j=0; j<HKL_LIST_LEN(mode->parameters); ++j){
				char const *name;
				double value;

				name = mode->parameters[j].name;
				value = hkl_parameter_get_value_unit(&mode->parameters[j]);
				snprintf(line, 255, "%s=%f", name, value);
				lines.push_back(line);
			}

			// Try to create property
			// Get crystal name
			Tango::DbDatum property(mode->name);
			property << lines;
			data_put.push_back(property);
		}

		//update database for this property
		_device->get_db_device()->put_attribute_property(data_put);
	}

	void PseudoAxesAdapter::load_1(void)
	{
		unsigned long nb_properties;

		// Get the Crystal Attributes properties.
		Tango::DbData properties;
		properties.push_back(Tango::DbDatum("Mode"));
		_device->get_db_device()->get_attribute_property(properties);

		// the first one is the number of properties
		properties[0] >> nb_properties;

		if (nb_properties > 1) {
			size_t i;
			HklPseudoAxisEngine *engine_r, *engine_w, *engine_r_real, *engine_w_real;

			engine_r = _hklAdapter->diffractometer()->engines_r->engines[_idx];
			engine_w = _hklAdapter->diffractometer()->engines_w->engines[_idx];
			engine_r_real = _hklAdapter->diffractometer()->engines_r_real->engines[_idx];
			engine_w_real = _hklAdapter->diffractometer()->engines_w_real->engines[_idx];

			for(i=1; i<=nb_properties; ++i) {
				size_t idx_m, j, len;
				char const *name;
				std::vector<std::string> lines;

				// first find the PseudoAxisEngineMode idx from its name
				name = properties[i].name.c_str();
				len = HKL_LIST_LEN(engine_r->modes);
				for(idx_m=0; idx_m<len; ++idx_m)
					if(!strcmp(name, engine_r->modes[idx_m]->name))
						break;
				if(idx_m>=len) // did not find the right mode name
					continue;

				// Extract the mode parameters from the properties
				properties[i] >> lines;

				// extract the parameters values
				for(j=0; j<lines.size(); j++) {
					size_t idx_p;

					char *line = strdup(lines[j].c_str());
					char *last;
					char *key = strtok_r(line, "=", &last);

					for(idx_p=0; idx_p<len; ++idx_p)
						if(!strcmp(key, engine_r->modes[idx_m]->parameters[idx_p].name))
							break;
					if(idx_p < len){
						double value;

						value = atof(strtok_r(NULL,";", &last));
						hkl_parameter_set_value_unit(&engine_r->modes[idx_m]->parameters[idx_p], value);
						hkl_parameter_set_value_unit(&engine_w->modes[idx_m]->parameters[idx_p], value);
						hkl_parameter_set_value_unit(&engine_r_real->modes[idx_m]->parameters[idx_p], value);
						hkl_parameter_set_value_unit(&engine_w_real->modes[idx_m]->parameters[idx_p], value);
					}
					free(line);
				}
			}// End for each property
		}
	}
}
