#include <iostream>
#include "PseudoAxesAttrib.h"
#include "PseudoAxes/PseudoAxes.h"

namespace Diffractometer_ns
{
	/******************/
	/* PseudoAxesAxis */
	/******************/

	PseudoAxesAxisAttrib::PseudoAxesAxisAttrib(char const *name, PseudoAxesAdapter & adapter, size_t idx):
		Tango::Attr(name, Tango::DEV_DOUBLE, Tango::READ_WRITE),
		_adapter(adapter),
		_idx(idx)
	{
		Tango::UserDefaultAttrProp axis_prop;
		axis_prop.set_label(name);
		axis_prop.set_format("%7.4f");

		std::string description;
		description += "The ";
		description += name;
		description += " pseudo axes axis of the diffractometer";
		axis_prop.set_description(description.c_str());
		axis_prop.set_unit("°");
		this->set_default_properties(axis_prop);
	}

	void PseudoAxesAxisAttrib::read(Tango::DeviceImpl *dev, Tango::Attribute &att)
	{
		PseudoAxes_ns::PseudoAxes *device = (PseudoAxes_ns::PseudoAxes *)dev;

		// use Tango to aknowledge for the write part.
		Tango::WAttribute & att_w = dev->get_device_attr()->get_w_attr_by_name(att.get_name().c_str());
		att.set_value(&device->config.read.data[_idx]);
		att_w.set_write_value(device->config.write.data[_idx]);
	}

	void PseudoAxesAxisAttrib::write(Tango::DeviceImpl *dev, Tango::WAttribute &att)
	{
		double value;
		att.get_write_value(value);
		_adapter.set_axis_value(_idx, value);
	}

	bool PseudoAxesAxisAttrib::is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType type)
	{
		if (dev->get_state() == Tango::FAULT ||
				dev->get_state() == Tango::ALARM)
			if (type == Tango::READ_REQ)
				return true;
			else
				return false;
		return true;
	}

} //- end namespace
