#include "Diffractometer.h"
#include "AxisAttrib.h"
#include "PseudoAxesAttrib.h"
#include "macros.h"

namespace Diffractometer_ns
{
	void Diffractometer::refresh_crystal_parameters()
	{
		DEBUG_STREAM << "Diffractometer::refresh_crystal_parameters(): entering... !" << endl;

		// Refresh Values
		if(_hklAdapter)
			_hklAdapter->get_sample_lattices(attr_A_read, attr_B_read, attr_C_read,
							 attr_Alpha_read, attr_Beta_read, attr_Gamma_read,
							 attr_AStar_read, attr_BStar_read, attr_CStar_read,
							 attr_AlphaStar_read, attr_BetaStar_read, attr_GammaStar_read);
	}

	/* this method return false if everythings goes fine, true otherwise */
	bool Diffractometer::get_type_from_property(std::string const & name, HklGeometryType &type)
	{
		bool res = false;

		if (!strcasecmp(name.c_str(), "2CV"))
			type = HKL_GEOMETRY_TYPE_TWOC_VERTICAL;
		else if (!strcasecmp(name.c_str(), "E4CV"))
			type = HKL_GEOMETRY_TYPE_EULERIAN4C_VERTICAL;
		else if (!strcasecmp(name.c_str(), "K4CV"))
			type = HKL_GEOMETRY_TYPE_KAPPA4C_VERTICAL;
		else if (!strcasecmp(name.c_str(), "K6C"))
			type = HKL_GEOMETRY_TYPE_KAPPA6C;
		else if (!strcasecmp(name.c_str(), "E6C"))
			type = HKL_GEOMETRY_TYPE_EULERIAN6C;
		else if (!strcasecmp(name.c_str(), "ZAXIS"))
			type = HKL_GEOMETRY_TYPE_ZAXIS;
		else
			res = true;

		return res;
	}

	void Diffractometer::create_dynamic_attributes(void)
	{
		if(_hklAdapter)
			_hklAdapter->attach_dynamic_attributes_to_device();
	}

	void Diffractometer::remove_dynamic_attributes(void)
	{
		if(_hklAdapter)
			_hklAdapter->detach_dynamic_attributes_from_device();
	}
} // namespace_ns
