#include "TangoHKLAdapter.h"
#include "PseudoAxisAdapter.h"
#include "macros.h"

namespace Diffractometer_ns
{

	PseudoAxisAdapter::PseudoAxisAdapter(TangoHKLAdapter &hklAdapter, HklPseudoAxis *pseudo_r, HklPseudoAxis *pseudo_w):
		_hklAdapter(hklAdapter),
		_pseudo_r(pseudo_r),
		_pseudo_w(pseudo_w)
	{
		size_t len;
		size_t i, j;
		char const **names;
		std::vector<AxisAdapter> & axes = hklAdapter.get_axes();

		_ready = false;
		_config.state = Tango::UNKNOWN;
		_config.mode = "";
		_config.initialized = false;
		_config.state = Tango::FAULT;
		_config.status = "Not yet initialized";

		// compute once for all the _mode_names attribute.
		len = HKL_LIST_LEN(_pseudo_r->engine->modes);
		_config.mode_names.resize(len, 1);
		for(i=0; i<len; ++i)
			_config.mode_names.data[i] = const_cast<char *>(_pseudo_r->engine->modes[i]->name);

		_axes.clear();
		len = HKL_LIST_LEN(_pseudo_r->engine->mode->axes_names);
		names = _pseudo_r->engine->mode->axes_names;
		for(i=0; i<len; ++i)
			for(j=0; j<axes.size(); ++j){
				AxisAdapter & axis = axes[j];
				if(axis.get_name() == names[i]) {
					_axes.push_back(&axis);
					continue;
				}
			}
	}

	PseudoAxisAdapter::~PseudoAxisAdapter(void)
	{
	}

	void PseudoAxisAdapter::update(void)
	{
		HklDiffractometer *diffractometer = _hklAdapter.diffractometer();

		// compute the positions
		_config.position_r =  hkl_parameter_get_value_unit((HklParameter *)_pseudo_r) + _config.offset;
		_config.position_w =  hkl_parameter_get_value_unit((HklParameter *)_pseudo_w) + _config.offset;

		// the internal part
		if(!diffractometer || !diffractometer->samples){
			_config.state = Tango::FAULT;
			_config.status = "Internal Error";
			return;
		}

		// the sample part
		if(!diffractometer->samples->current){
			_config.state = Tango::FAULT;
			_config.status = "Sample not set";
			return;
		}

		// now compute the state ans status of the pseudo axes.
		_config.state = Tango::STANDBY;
		std::string extra_status;

		_config.status = "REAL Mode: Motors will REALLY move.";
		if (!this->is_ready()) {
			_config.state = Tango::FAULT;
			extra_status = "\nCan not connect to axes proxies";
		} else {
			for(size_t i=0;i<_axes.size();++i) {
				AxisAdapter const * axis = _axes[i];
				::compose_state(_config.state, axis->get_state());
				if (axis->get_state() != Tango::STANDBY)
					extra_status += "\n" + axis->get_proxy_name() + " is in " + Tango::DevStateName[axis->get_state()];
			}
		}
		extra_status += "\nPseudoAxe status: ";
		extra_status += Tango::DevStateName[_config.state];
		_config.status += extra_status;

		// update the mode
		_config.mode = const_cast<Tango::DevString>(this->_pseudo_r->engine->mode->name);

		// initialized part
		if(this->_pseudo_r->engine->mode->initialize == NULL)
			_config.initialized = true;
	}

	PseudoAxisConfig PseudoAxisAdapter::get_config(void)
	{
		return _hklAdapter.pseudo_axis_get_config(this);
	}

	void PseudoAxisAdapter::to_proxies(void)
	{
#ifdef WRITE_TO_PROXY_ALLOWED
		if(this->is_ready())
			for (size_t i=0; i<_axes.size(); ++i) {
				_axes[i]->stop();
				_axes[i]->from_HklAxis();
				_axes[i]->set_state(Tango::MOVING);
				_axes[i]->to_proxy();
			}
#endif
	}

	bool PseudoAxisAdapter::is_ready(void)
	{
		if(!_ready){
			size_t i;

			for(i=0; i<_axes.size(); ++i)
				if(!_axes[i]->is_ready())
					return _ready;
			_ready = true;
		}
		return _ready;
	}

	void PseudoAxisAdapter::set_position(const Tango::DevDouble & position)
	{
		_hklAdapter.pseudo_axis_set_position(this, position);
	}

	void PseudoAxisAdapter::set_offset(const Tango::DevDouble & offset)
	{
		_config.offset = offset;
	}
 
	void PseudoAxisAdapter::set_mode(Tango::DevString const & mode)
	{
		_hklAdapter.pseudo_axis_set_mode(this, mode);
	}

	void PseudoAxisAdapter::get_mode_parameters(Tango::DevVarDoubleStringArray *argout)
	{
		_hklAdapter.pseudo_axis_get_mode_parameters(this, argout);
	}

	void PseudoAxisAdapter::set_mode_parameters(const Tango::DevVarDoubleStringArray *argin)
	{
		_hklAdapter.pseudo_axis_set_mode_parameters(this, argin);
	}

	void PseudoAxisAdapter::set_initialized(Tango::DevBoolean initialized)
	{
		_hklAdapter.pseudo_axis_set_initialized(this, initialized);
	}

	Tango::DevDouble PseudoAxisAdapter::compute_new_offset(const Tango::DevDouble & position)
	{
		return position - hkl_parameter_get_value_unit((HklParameter *)_pseudo_r);
	}

	void PseudoAxisAdapter::on(void)
	{
		_hklAdapter.pseudo_axis_on(this);
	}

	void PseudoAxisAdapter::stop(void)
	{
#ifdef WRITE_TO_PROXY_ALLOWED
		if(this->is_ready())
			for(size_t i=0; i<_axes.size(); ++i)
				_axes[i]->stop();
#endif
	}
}
