#ifndef _MACROS_H
#define _MACROS_H

#include <tango.h>

#ifdef _MSC_VER
# define strtok_r strtok_s
# define snprintf _snprintf
# define strcasecmp _stricmp
#endif

#define AXIS_ATTRIBUTE_POSITION_NAME "position"
#define AXIS_COMMAND_STOP_NAME "Stop"

#define MAX_LENGTH_COMPUTED_ANGLES 1000

// Comment the next line to forbide the proxy write part
//#define WRITE_TO_PROXY_ALLOWED

// version of the save/load format method
#define FORMAT_VERSION 1

#define AVAILABLE_DIFFRACTOMETER_TYPE "2CV, E4CV, K4CV, E6C, K6C, ZAXIS"

#define XSTR(s) STR(s)
#define STR(s) #s

#define TANGO_EXCEPTION_RE_THROW_(tango_ex, b) do{\
	std::string location(__FILE__);\
	location += " line : ";\
	location += XSTR(__LINE__);\
	ERROR_STREAM << (b) << tango_ex << location;\
	Tango::Except::re_throw_exception(tango_ex,\
		static_cast<const char*>(b),\
		static_cast<const char*>(""),\
		static_cast<const char*>(location.c_str()));\
} while(0);

#define TANGO_EXCEPTION_RE_THROW_WITHOUT_LOG_(tango_ex, b) do{\
	std::string location(__FILE__);\
	location += " line : ";\
	location += XSTR(__LINE__);\
	Tango::Except::re_throw_exception(tango_ex,\
		static_cast<const char*>(b),\
		static_cast<const char*>(""),\
		static_cast<const char*>(location.c_str()));\
} while(0);

#define TANGO_EXCEPTION_THROW(a,b)  do {\
	std::string location(__FILE__);\
	location += " line : ";\
	location += XSTR(__LINE__);\
	ERROR_STREAM << (a) << (b) << location;\
	Tango::Except::throw_exception(static_cast<const char*>((a)),\
			static_cast<const char *>((b)),\
			static_cast<const char *>(location.c_str()));\
} while(0);

#define TANGO_EXCEPTION_THROW_WITHOUT_LOG(a,b)  do {\
	std::string location(__FILE__);\
	location += " line : ";\
	location += XSTR(__LINE__);\
	Tango::Except::throw_exception(static_cast<const char*>((a)),\
			static_cast<const char *>((b)),\
			static_cast<const char *>(location.c_str()));\
} while(0);

#define TANGO_EXCEPTION_RE_THROW(tango_ex) do{\
	TANGO_EXCEPTION_RE_THROW_(tango_ex, "TANGO_ERROR");\
} while(0);

inline void compose_state(Tango::DevState & state, Tango::DevState const & axe_state)
{
	switch(axe_state) {
	case Tango::OFF:
		if (state == Tango::MOVING || state == Tango::STANDBY)
			state = Tango::OFF;
		break;
	case Tango::MOVING:
		if (state == Tango::STANDBY)
			state = Tango::MOVING;
		break;
	case Tango::FAULT:
		state = Tango::FAULT;
		break;
	default:
		break;
	}
}

#endif // _MACROS_H
