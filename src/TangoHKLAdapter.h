#ifndef __TANGOHKLADAPTER_H__
#define __TANGOHKLADAPTER_H__

#include <map>
#include <vector>
#include <string>
#include <tango.h>

#include <Duration.h>

#include <hkl.h>

#include "AxisAdapter.h"
#include "PseudoAxisAdapter.h"
#include "PseudoAxesAdapter.h"
#include "Matrix.h"

#if ( _MSC_VER && _MSC_VER <= 1200 )
// Ce pragma ne devrait pas être dans ce fichier car le warning ne s'affiche
// que si l'on compile une version DEBUG de la librairie.
// Il faut utiliser l'option de compilation \wd4786 mais elle n'est présente
// qu'à partir de VC++7
# pragma warning (disable : 4786)
#endif

#define NO_SOFTWARE_LIMITS -999999

namespace Diffractometer_ns {

	// forward declaration
	class Diffractometer;

	struct HklDiffractometer {
		HklGeometry *geometry_r;
		HklGeometry *geometry_w;
		HklGeometry *geometry_r_real;
		HklGeometry *geometry_w_real;
		HklDetector *detector;
		HklDetector *detector_real;
		HklSampleList *samples;
		HklPseudoAxisEngineList *engines_r;
		HklPseudoAxisEngineList *engines_w;
		HklPseudoAxisEngineList *engines_r_real;
		HklPseudoAxisEngineList *engines_w_real;
	};

	struct DiffractometerConfig {
		Tango::DevState state;
		std::string status;
	};

	/**
	 * 
	 * Class which associates a Diffractometer with the PseudoAxis
	 */
	class TangoHKLAdapter
	{
	public :
		bool _auto_update_from_proxies;
		bool ready;

		TangoHKLAdapter(Diffractometer *_device, HklGeometryType type);

		virtual ~TangoHKLAdapter(void);

		Diffractometer *get_device(void) {return _device;}

		// Diffractometer Part
		HklDiffractometer* & diffractometer(void) {return _diffractometer;}

		void get_diffractometer_config(DiffractometerConfig &config);

		
		bool get_auto_update_from_proxies(void);
		void set_auto_update_from_proxies(bool update);

		void connect_all_proxies(void);

		void update(void);

		/**********/
		/* source */
		/**********/

		double & get_lambda(void) {return _lambda;}

		void set_lambda(double lambda);

		/************/
		/* hkl part */
		/************/

		Matrix<double> & get_angles(void) {omni_mutex_lock lock(_lock); return _angles;}

		short & get_angles_idx(void);

		void set_angles_idx(short idx);

		void load(void);

		void save(void);

		/***************/
		/* sample part */
		/***************/

		char const *get_sample_name(void);

		void set_current_sample(char const * name);

		void get_sample_lattices(double * a, double * b, double * c,
					 double * alpha, double * beta, double * gamma,
					 double * a_star, double * b_star, double * c_star,
					 double * alpha_star, double * beta_star, double * gamma_star); 

		void get_sample_fit(bool *afit, bool *bfit, bool *cfit,
				    bool *alphafit, bool *betafit, bool *gammafit,
				    bool *uxfit, bool *uyfit, bool *uzfit);

		Matrix<double> & get_sample_ub(void) {omni_mutex_lock lock(_lock); return _ub;}

		double & get_sample_Ux(void) {return _ux;}

		void set_sample_Ux(double ux);

		double & get_sample_Uy(void) {return _uy;}

		void set_sample_Uy(double uy);

		double & get_sample_Uz(void) {return _uz;}

		void set_sample_Uz(double uz);

		void set_sample_AFit(bool fit);
		void set_sample_BFit(bool fit);
		void set_sample_CFit(bool fit);
		void set_sample_AlphaFit(bool fit);
		void set_sample_BetaFit(bool fit);
		void set_sample_GammaFit(bool fit);
		void set_sample_UxFit(bool fit);
		void set_sample_UyFit(bool fit);
		void set_sample_UzFit(bool fit);

		void add_new_sample(std::string const & name);

		void copy_sample_as(Tango::DevString copy_name);

		void del_sample(void);

		void set_lattice(const Tango::DevVarDoubleArray *argin);

		void add_reflection(const Tango::DevVarDoubleArray *argin);

		void del_reflection(Tango::DevShort argin);

		Matrix<double> & get_reflections(void) {omni_mutex_lock lock(_lock); return _reflections;}

		void set_reflections(Matrix<double> const & img);

		double affine_sample(std::string name);

		std::vector<std::string> get_samples_names(void);

		void get_sample_parameter_values(Tango::DevVarDoubleStringArray *argout);

		void set_sample_parameter_values(Tango::DevVarDoubleStringArray const *argin);

		void compute_u(const Tango::DevVarLongArray *argin);

		Matrix<double> & get_reflections_angles(void) {omni_mutex_lock lock(_lock); return _reflections_angles;}

		/*************/
		/* axes part */
		/*************/

		const std::vector<AxisAdapter> & get_axes(void) const {return _axes;}

		std::vector<AxisAdapter> & get_axes(void) {return _axes;}

		void read_axis(int idx, double & read, double & write);

		void write_axis(AxisAdapter & adapter, double value);

		void stop_all_axis(void);

		/*******************/
		/* PseudoAxis Part */
		/*******************/

		std::vector<std::string> pseudo_axis_get_names(void);

		PseudoAxisAdapter *pseudo_axis_buffer_new(char const *name);

		void pseudo_axis_set_position(PseudoAxisAdapter *buffer,
					      const Tango::DevDouble & position);

		PseudoAxisConfig pseudo_axis_get_config(PseudoAxisAdapter *buffer);

		void pseudo_axis_set_mode(PseudoAxisAdapter *buffer, Tango::DevString const & mode);

		void pseudo_axis_get_mode_parameters(PseudoAxisAdapter *buffer, Tango::DevVarDoubleStringArray *argout);

		void pseudo_axis_set_mode_parameters(PseudoAxisAdapter *buffer, const Tango::DevVarDoubleStringArray *argin);

		void pseudo_axis_set_initialized(PseudoAxisAdapter *buffer, Tango::DevBoolean initialized);

		void pseudo_axis_on(PseudoAxisAdapter *buffer);

		/*******************/
		/* PseudoAxes Part */
		/*******************/

		std::vector<PseudoAxesAdapter *> & pseudo_axes(void) {return _pseudoAxesAdapters;}

		PseudoAxesAdapter *pseudo_axes_adapter_get_by_name(std::string const & name);

		PseudoAxesConfig pseudo_axes_get_config(size_t idx);

		Matrix<char *> const & get_pseudo_axes_proxies(void) {omni_mutex_lock lock(_lock); return _pseudo_axes_proxies;}

		void pseudo_axes_set_axis_value(size_t idx, size_t p_idx, double value);

		void pseudo_axes_apply(size_t idx, Matrix<double> const & write) throw (Tango::DevFailed);

		void pseudo_axes_set_mode(size_t idx, const Tango::DevString name);

		void pseudo_axes_set_initialized(size_t idx, const Tango::DevBoolean initialized);

		void pseudo_axes_set_mode_parameters(size_t idx, Matrix<double> const & values);

		void pseudo_axes_add_dynamic_attributes(size_t idx);

		void pseudo_axes_remove_dynamic_attributes(size_t idx);

		void pseudo_axes_init(size_t idx);
		
		void pseudo_axes_create_and_start_devices(void);

		/**********************/
		/* Dynamic attributes */
		/**********************/

		void create_axes_dynamic_attributes(void);

		void destroy_axes_dynamic_attributes(void);

		void attach_dynamic_attributes_to_device(void);

		void detach_dynamic_attributes_from_device(void);

		Matrix<char *> const & get_dynamic_attribute_axes_names(void) const {return _dynamic_attribute_axes_names;}

	protected :

		omni_mutex _lock;
		HklDiffractometer *_diffractometer;
		DiffractometerConfig _diffractometerConfig;
		std::vector<AxisAdapter> _axes;
		std::vector<PseudoAxisAdapter *> _pseudoAxisAdapters;
		std::vector<PseudoAxesAdapter *> _pseudoAxesAdapters;
		Tango::AttributeProxy *_lambdaAttributeProxy;
		bool _wrong_nb_of_axis_proxies;

		void update_pseudo_axis_engines(void);

		void update_lambda(void);

		void update_angles(void);

		void update_reflections_angles(void);

		void update_reflections(void);

		void update_ub(void);

		void update_ux_uy_uz(void);

		void update_axis_adapters(void);

		void update_pseudo_axis_adapters_from_hkl(void);

		void update_pseudo_axes_adapters_from_hkl(void);

		void update_hkl_from_axis_adapters(void);

		void update_proxies_from_axis_adapters(void);

		void update_proxies_from_pseudo_axis_adapters(PseudoAxisAdapter *adapter);

		void update_state_and_status(void);

		void load_1(void);

		/* axis part */
		void write_axis_i(AxisAdapter & axis, double value);

	private :

		Diffractometer *_device;
		HklGeometryType _type;
		Duration duration;
		double _lambda;
		Matrix<double> _angles;
		Matrix<char *> _pseudo_axes_proxies;
		short _angles_idx;

		// sample part
		Matrix<double> _reflections_angles;
		Matrix<double> _reflections;
		Matrix<double> _ub;
		double _ux;
		double _uy;
		double _uz;
		bool _uxfit;
		bool _uyfit;
		bool _uzfit;

		// dynamic attributes part
		std::vector<Tango::Attr *> _dynamic_attribute_axes;
		Matrix<char *> _dynamic_attribute_axes_names;
	};
}

// given the state compute the new state using the axe state.
extern void compose_state(Tango::DevState & state, Tango::DevState const & axe_state);

#endif// _TangoHKLAdapter_H
