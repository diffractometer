#ifndef _PSEUDO_AXES_ATTRIB_H_
#define _PSEDUO_AXES_ATTRIB_H_

#include <tango.h>
#include <Diffractometer.h>

namespace Diffractometer_ns
{
	class PseudoAxesAxisAttrib : public Tango::Attr
	{
	public:
		PseudoAxesAxisAttrib(char const *name,
				     PseudoAxesAdapter & adapter,
				     size_t idx);

		virtual ~PseudoAxesAxisAttrib(void) {};

		virtual void read(Tango::DeviceImpl *dev,
				  Tango::Attribute &att);

		virtual void write(Tango::DeviceImpl *dev,
				   Tango::WAttribute &att);

		virtual bool is_allowed(Tango::DeviceImpl *dev,
					Tango::AttReqType ty);

	protected:
		PseudoAxesAdapter & _adapter;
		size_t _idx;
	};

} // namespace Diffractometer_ns

#endif // _PSEUDO_AXES_ATTRIB_H_
