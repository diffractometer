#include <tango.h>
#include <PseudoAxisClass.h>
#include <PseudoAxesClass.h>
#include <DiffractometerClass.h>

/**
 *	Create DiffractometerClass singleton and store it in DServer object.
 *
 * @author	$Author: piccaf $
 * @version	$Revision: 1.2 $ $
 */

void Tango::DServer::class_factory()
{
  add_class(Diffractometer_ns::DiffractometerClass::init("Diffractometer"));
  add_class(PseudoAxis_ns::PseudoAxisClass::init("PseudoAxis"));
  add_class(PseudoAxes_ns::PseudoAxesClass::init("PseudoAxes"));
}
