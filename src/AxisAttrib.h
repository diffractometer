#ifndef _DYNAMIC_ATTR_AXIS_H_
#define _DYNAMIC_ATTR_AXIS_H_

#include <tango.h>
#include <Diffractometer.h>

namespace Diffractometer_ns
{
	class AxisAttrib : public Tango::Attr
	{
	public:
		AxisAttrib(char const *name, AxisAdapter & adapter);

		virtual ~AxisAttrib(void) {};

		virtual void read(Tango::DeviceImpl *dev,
				  Tango::Attribute &att);

		virtual void write(Tango::DeviceImpl *dev,
				   Tango::WAttribute &att);

		virtual bool is_allowed(Tango::DeviceImpl *dev,
					Tango::AttReqType ty);

	protected:
		AxisAdapter & _adapter;
	};

} // namespace Diffractometer_ns

#endif // _DYNAMIC_ATTR_AXIS_H_
