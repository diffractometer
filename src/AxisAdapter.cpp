#include "AxisAdapter.h"
#include "TangoHKLAdapter.h"
#include "macros.h"

namespace Diffractometer_ns {

	AxisAdapter::AxisAdapter(TangoHKLAdapter * hklAdapter, HklAxis *axis_r, HklAxis *axis_w,
				 HklAxis *axis_r_real, HklAxis *axis_w_real) :
		_hklAdapter(hklAdapter),
		_axis_r(axis_r),
		_axis_w(axis_w),
		_axis_r_real(axis_r_real),
		_axis_w_real(axis_w_real)
	{
		_proxy_name = "";
		_proxy_stop_command_name = "Stop";
		_proxy_on_command_name = "On";
		_proxy = NULL;
		this->from_HklAxis();
		_state = Tango::UNKNOWN;
	}

	AxisAdapter::~AxisAdapter(void)
	{
	}

	std::string const AxisAdapter::get_name(void) const
	{
		return ((HklParameter *)(_axis_r))->name;
	}

	std::string const AxisAdapter::get_proxy_name(void) const
	{
		return _proxy_name;
	}

	bool AxisAdapter::connect(char *proxy_name)
	{
		bool res = false;

		_proxy_name = proxy_name;
		try{
			_proxy = new Tango::AttributeProxy(proxy_name);
			if(_proxy){
				std::vector<double> positions;

				res = true;

				// read the read/write position
				_proxy->read() >> positions;
				hkl_axis_set_value_unit(_axis_r, positions[0]);
				hkl_axis_set_value_unit(_axis_w, positions[1]);
				_read = positions[0];
				_write = positions[1];

				// now read the range
				std::string attr_name = AXIS_ATTRIBUTE_POSITION_NAME;
				Tango::AttributeInfo info = _proxy->get_config();

				// Check if min and max value have been set
				char const *token;
				token = info.min_value.c_str();
				if (!strstr(token, "Not specified"))
					_min = atof(token);

				token = info.max_value.c_str();
				if (!strstr(token, "Not specified"))
					_max = atof(token);
			}
		}catch(Tango::DevFailed &){
			_state = Tango::FAULT;
		}
		return res;
	}

	bool const AxisAdapter::is_ready(void) const
	{
		if (_proxy)
			return true;
		else
			return false;
	}

	void AxisAdapter::write(double value)
	{
		_hklAdapter->write_axis(*this, value);
	}

	void AxisAdapter::from_HklAxis(void)
	{
		_read = hkl_axis_get_value_unit(_axis_r);
		_write = hkl_axis_get_value_unit(_axis_w);
		_read_real = hkl_axis_get_value_unit(_axis_r_real);
		_write_real = hkl_axis_get_value_unit(_axis_w_real);
		hkl_axis_get_range_unit(_axis_r, &_min, &_max);
	}

	void AxisAdapter::to_HklAxis(void)
	{
		hkl_axis_set_value_unit(_axis_r, _read);
		hkl_axis_set_value_unit(_axis_w, _write);
		hkl_axis_set_range_unit(_axis_r, _min, _max);
		hkl_axis_set_range_unit(_axis_w, _min, _max);
		hkl_axis_set_value_unit(_axis_r_real, _read_real);
		hkl_axis_set_value_unit(_axis_w_real, _write_real);
		hkl_axis_set_range_unit(_axis_r_real, _min, _max);
		hkl_axis_set_range_unit(_axis_w_real, _min, _max);
	}

	void AxisAdapter::from_proxy(bool simulated)
	{
		try{
			if(this->is_ready()){
				std::vector<double> positions;

				// read the position attribute
				_proxy->read() >> positions;
				_read_real = positions[0];
				_write_real = positions[1];

				Tango::AttributeInfo info = _proxy->get_config();

				// Check if min and max value have been set
				char const *token;
				token = info.min_value.c_str();
				if (!strstr(token, "Not specified"))
					_min = atof(token);

				token = info.max_value.c_str();
				if (!strstr(token, "Not specified"))
					_max = atof(token);

				// read the state
				_state = _proxy->state();

				if(!simulated){
					_read = _read_real;
					_write = _write_real;
				}else{
					_read = hkl_axis_get_value_unit(_axis_r);
					_write = hkl_axis_get_value_unit(_axis_w);
				}
			}
		}
		catch(Tango::DevFailed &)
		{
			_state = Tango::FAULT;
		}
	}

	void AxisAdapter::to_proxy(void)
	{
#ifdef WRITE_TO_PROXY_ALLOWED
		try{
			if(this->is_ready()){
				Tango::DeviceAttribute attr(_proxy_name, _write_real);
				_state = Tango::MOVING;
				_proxy->write(attr);
			}
		}
		catch(Tango::DevFailed &)
		{
			_state = Tango::FAULT;
		}
#endif
	}

	void AxisAdapter::stop(void)
	{
		// let the error propagate if stop failed.
		if(this->is_ready())
			_proxy->get_device_proxy()->command_inout(_proxy_stop_command_name);
	}

	void AxisAdapter::on(void)
	{
		if(this->is_ready())
			try{
				_proxy->get_device_proxy()->command_inout(_proxy_on_command_name);
			}
			catch (Tango::DevFailed &)
			{
				_state = Tango::FAULT;
				// do nothing if the command is not present.
			}
	}
}
