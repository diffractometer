//=============================================================================
//
// file :        Diffractometer.h
//
// description : Include for the Diffractometer class.
//
// project :	Diffractometer
//
// $Author: piccaf $
//
// $Revision: 1.3 $
//
// $Log: Diffractometer.h,v $
// Revision 1.3  2008/10/16 10:16:55  piccaf
// * update to build on windows with vc8
// * add the Eulerian6C diffractometer.
// * add the dynamic attributes in the factory instead of the init device
//
// Revision 1.2  2008/09/22 08:46:23  piccaf
// * add files from the new diffractometer Device
//
//
// copyleft :    European Synchrotron Radiation Facility
//               BP 220, Grenoble 38043
//               FRANCE
//
//=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================
#ifndef _DIFFRACTOMETER_H
#define _DIFFRACTOMETER_H

#include <tango.h>
//using namespace Tango;

/**
 * @author	$Author: piccaf $
 * @version	$Revision: 1.3 $
 */

 //	Add your own constants definitions here.
 //-----------------------------------------------

#include "PogoHelper.h"
#include "TangoHKLAdapter.h"

namespace Diffractometer_ns
{

/**
 * Class Description:
 * This class can be used to compute cristallography computation
 */

/*
 *	Device States Description:
*  Tango::FAULT :    HKLlibrary not initialised or at least 1 of the motor is in FAULT state
*  Tango::ALARM :    At least 1 of the motor is in ALARM state
*  Tango::STANDBY :  All motors are in STANDBY state
*  Tango::MOVING :   At least one motor is in MOVING state
*  Tango::OFF :      At least one motor is in OFF state
 */


class Diffractometer: public Tango::Device_4Impl
{
public :
	//	Add your own data members here
	//-----------------------------------------


	//	Here is the Start of the automatic code generation part
	//-------------------------------------------------------------	
/**
 *	@name attributes
 *	Attributs member data.
 */
//@{
		Tango::DevShort	*attr_AnglesIdx_read;
		Tango::DevShort	attr_AnglesIdx_write;
		Tango::DevBoolean	*attr_AutoUpdateFromProxies_read;
		Tango::DevBoolean	attr_AutoUpdateFromProxies_write;
		Tango::DevDouble	*attr_WaveLength_read;
		Tango::DevDouble	attr_WaveLength_write;
		Tango::DevString	*attr_Crystal_read;
		Tango::DevString	attr_Crystal_write;
		Tango::DevDouble	*attr_A_read;
		Tango::DevBoolean	*attr_AFit_read;
		Tango::DevBoolean	attr_AFit_write;
		Tango::DevDouble	*attr_B_read;
		Tango::DevBoolean	*attr_BFit_read;
		Tango::DevBoolean	attr_BFit_write;
		Tango::DevDouble	*attr_C_read;
		Tango::DevBoolean	*attr_CFit_read;
		Tango::DevBoolean	attr_CFit_write;
		Tango::DevDouble	*attr_Alpha_read;
		Tango::DevBoolean	*attr_AlphaFit_read;
		Tango::DevBoolean	attr_AlphaFit_write;
		Tango::DevDouble	*attr_Beta_read;
		Tango::DevBoolean	*attr_BetaFit_read;
		Tango::DevBoolean	attr_BetaFit_write;
		Tango::DevDouble	*attr_Gamma_read;
		Tango::DevBoolean	*attr_GammaFit_read;
		Tango::DevBoolean	attr_GammaFit_write;
		Tango::DevDouble	*attr_Ux_read;
		Tango::DevDouble	attr_Ux_write;
		Tango::DevBoolean	*attr_UxFit_read;
		Tango::DevBoolean	attr_UxFit_write;
		Tango::DevDouble	*attr_Uy_read;
		Tango::DevDouble	attr_Uy_write;
		Tango::DevBoolean	*attr_UyFit_read;
		Tango::DevBoolean	attr_UyFit_write;
		Tango::DevDouble	*attr_Uz_read;
		Tango::DevDouble	attr_Uz_write;
		Tango::DevBoolean	*attr_UzFit_read;
		Tango::DevBoolean	attr_UzFit_write;
		Tango::DevDouble	*attr_AStar_read;
		Tango::DevDouble	*attr_BStar_read;
		Tango::DevDouble	*attr_CStar_read;
		Tango::DevDouble	*attr_AlphaStar_read;
		Tango::DevDouble	*attr_BetaStar_read;
		Tango::DevDouble	*attr_GammaStar_read;
		Tango::DevBoolean	*attr_Simulated_read;
		Tango::DevBoolean	attr_Simulated_write;
		Tango::DevString	*attr_CrystalNames_read;
		Tango::DevString	*attr_AxesNames_read;
		Tango::DevString	*attr_pseudoAxesProxies_read;
		Tango::DevDouble	*attr_Angles_read;
		Tango::DevBoolean	*attr_AnglesDegenerated_read;
		Tango::DevBoolean	*attr_AnglesRangeCheck_read;
		Tango::DevDouble	*attr_Reflections_read;
		Tango::DevDouble	attr_Reflections_write;
		Tango::DevDouble	*attr_ReflectionsAngles_read;
		Tango::DevDouble	*attr_UB_read;
//@}

/**
 *	@name Device properties
 *	Device properties member data.
 */
//@{
/**
 *	Definition of the Tau constant .
 *	Possible values are pi or 1 .
 *	Default : 1
 */
	Tango::DevDouble	tauConstant;
/**
 *	
 */
	vector<string>	realAxisProxies;
/**
 *	The name of the lambda attribute proxy.
 *	
 *	The diffractometer use it to read the lambda value from another Device
 */
	string	lambdaAttributeProxy;
/**
 *	The Type of the diffractometer
 *	
 *	
 */
	string	diffractometerType;
/**
 *	allow or not to change the values of the axes in the Reflections attribute.
 *	
 *	set this properties to false to allow writting on the axes values.
 */
	Tango::DevBoolean	protectReflectionAxes;
//@}

/**@name Constructors
 * Miscellaneous constructors */
//@{
/**
 * Constructs a newly allocated Command object.
 *
 *	@param cl	Class.
 *	@param s 	Device Name
 */
	Diffractometer(Tango::DeviceClass *cl,string &s);
/**
 * Constructs a newly allocated Command object.
 *
 *	@param cl	Class.
 *	@param s 	Device Name
 */
	Diffractometer(Tango::DeviceClass *cl,const char *s);
/**
 * Constructs a newly allocated Command object.
 *
 *	@param cl	Class.
 *	@param s 	Device name
 *	@param d	Device description.
 */
	Diffractometer(Tango::DeviceClass *cl,const char *s,const char *d);
//@}

/**@name Destructor
 * Only one desctructor is defined for this class */
//@{
/**
 * The object desctructor.
 */	
	~Diffractometer() {delete_device();};
/**
 *	will be called at device destruction or at init command.
 */
	void delete_device();
//@}

	
/**@name Miscellaneous methods */
//@{
/**
 *	Initialize the device
 */
	virtual void init_device();
/**
 *	Always executed method befor execution command method.
 */
	virtual void always_executed_hook();

//@}

/**
 * @name Diffractometer methods prototypes
 */

//@{
/**
 *	Hardware acquisition for attributes.
 */
	virtual void read_attr_hardware(vector<long> &attr_list);
/**
 *	Extract real attribute values for AnglesIdx acquisition result.
 */
	virtual void read_AnglesIdx(Tango::Attribute &attr);
/**
 *	Write AnglesIdx attribute values to hardware.
 */
	virtual void write_AnglesIdx(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for AutoUpdateFromProxies acquisition result.
 */
	virtual void read_AutoUpdateFromProxies(Tango::Attribute &attr);
/**
 *	Write AutoUpdateFromProxies attribute values to hardware.
 */
	virtual void write_AutoUpdateFromProxies(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for WaveLength acquisition result.
 */
	virtual void read_WaveLength(Tango::Attribute &attr);
/**
 *	Write WaveLength attribute values to hardware.
 */
	virtual void write_WaveLength(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for Crystal acquisition result.
 */
	virtual void read_Crystal(Tango::Attribute &attr);
/**
 *	Write Crystal attribute values to hardware.
 */
	virtual void write_Crystal(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for A acquisition result.
 */
	virtual void read_A(Tango::Attribute &attr);
/**
 *	Extract real attribute values for AFit acquisition result.
 */
	virtual void read_AFit(Tango::Attribute &attr);
/**
 *	Write AFit attribute values to hardware.
 */
	virtual void write_AFit(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for B acquisition result.
 */
	virtual void read_B(Tango::Attribute &attr);
/**
 *	Extract real attribute values for BFit acquisition result.
 */
	virtual void read_BFit(Tango::Attribute &attr);
/**
 *	Write BFit attribute values to hardware.
 */
	virtual void write_BFit(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for C acquisition result.
 */
	virtual void read_C(Tango::Attribute &attr);
/**
 *	Extract real attribute values for CFit acquisition result.
 */
	virtual void read_CFit(Tango::Attribute &attr);
/**
 *	Write CFit attribute values to hardware.
 */
	virtual void write_CFit(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for Alpha acquisition result.
 */
	virtual void read_Alpha(Tango::Attribute &attr);
/**
 *	Extract real attribute values for AlphaFit acquisition result.
 */
	virtual void read_AlphaFit(Tango::Attribute &attr);
/**
 *	Write AlphaFit attribute values to hardware.
 */
	virtual void write_AlphaFit(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for Beta acquisition result.
 */
	virtual void read_Beta(Tango::Attribute &attr);
/**
 *	Extract real attribute values for BetaFit acquisition result.
 */
	virtual void read_BetaFit(Tango::Attribute &attr);
/**
 *	Write BetaFit attribute values to hardware.
 */
	virtual void write_BetaFit(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for Gamma acquisition result.
 */
	virtual void read_Gamma(Tango::Attribute &attr);
/**
 *	Extract real attribute values for GammaFit acquisition result.
 */
	virtual void read_GammaFit(Tango::Attribute &attr);
/**
 *	Write GammaFit attribute values to hardware.
 */
	virtual void write_GammaFit(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for Ux acquisition result.
 */
	virtual void read_Ux(Tango::Attribute &attr);
/**
 *	Write Ux attribute values to hardware.
 */
	virtual void write_Ux(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for UxFit acquisition result.
 */
	virtual void read_UxFit(Tango::Attribute &attr);
/**
 *	Write UxFit attribute values to hardware.
 */
	virtual void write_UxFit(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for Uy acquisition result.
 */
	virtual void read_Uy(Tango::Attribute &attr);
/**
 *	Write Uy attribute values to hardware.
 */
	virtual void write_Uy(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for UyFit acquisition result.
 */
	virtual void read_UyFit(Tango::Attribute &attr);
/**
 *	Write UyFit attribute values to hardware.
 */
	virtual void write_UyFit(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for Uz acquisition result.
 */
	virtual void read_Uz(Tango::Attribute &attr);
/**
 *	Write Uz attribute values to hardware.
 */
	virtual void write_Uz(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for UzFit acquisition result.
 */
	virtual void read_UzFit(Tango::Attribute &attr);
/**
 *	Write UzFit attribute values to hardware.
 */
	virtual void write_UzFit(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for AStar acquisition result.
 */
	virtual void read_AStar(Tango::Attribute &attr);
/**
 *	Extract real attribute values for BStar acquisition result.
 */
	virtual void read_BStar(Tango::Attribute &attr);
/**
 *	Extract real attribute values for CStar acquisition result.
 */
	virtual void read_CStar(Tango::Attribute &attr);
/**
 *	Extract real attribute values for AlphaStar acquisition result.
 */
	virtual void read_AlphaStar(Tango::Attribute &attr);
/**
 *	Extract real attribute values for BetaStar acquisition result.
 */
	virtual void read_BetaStar(Tango::Attribute &attr);
/**
 *	Extract real attribute values for GammaStar acquisition result.
 */
	virtual void read_GammaStar(Tango::Attribute &attr);
/**
 *	Extract real attribute values for Simulated acquisition result.
 */
	virtual void read_Simulated(Tango::Attribute &attr);
/**
 *	Write Simulated attribute values to hardware.
 */
	virtual void write_Simulated(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for CrystalNames acquisition result.
 */
	virtual void read_CrystalNames(Tango::Attribute &attr);
/**
 *	Extract real attribute values for AxesNames acquisition result.
 */
	virtual void read_AxesNames(Tango::Attribute &attr);
/**
 *	Extract real attribute values for pseudoAxesProxies acquisition result.
 */
	virtual void read_pseudoAxesProxies(Tango::Attribute &attr);
/**
 *	Extract real attribute values for Angles acquisition result.
 */
	virtual void read_Angles(Tango::Attribute &attr);
/**
 *	Extract real attribute values for AnglesDegenerated acquisition result.
 */
	virtual void read_AnglesDegenerated(Tango::Attribute &attr);
/**
 *	Extract real attribute values for AnglesRangeCheck acquisition result.
 */
	virtual void read_AnglesRangeCheck(Tango::Attribute &attr);
/**
 *	Extract real attribute values for Reflections acquisition result.
 */
	virtual void read_Reflections(Tango::Attribute &attr);
/**
 *	Write Reflections attribute values to hardware.
 */
	virtual void write_Reflections(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for ReflectionsAngles acquisition result.
 */
	virtual void read_ReflectionsAngles(Tango::Attribute &attr);
/**
 *	Extract real attribute values for UB acquisition result.
 */
	virtual void read_UB(Tango::Attribute &attr);
/**
 *	Read/Write allowed for AnglesIdx attribute.
 */
	virtual bool is_AnglesIdx_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for AutoUpdateFromProxies attribute.
 */
	virtual bool is_AutoUpdateFromProxies_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for WaveLength attribute.
 */
	virtual bool is_WaveLength_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for Crystal attribute.
 */
	virtual bool is_Crystal_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for A attribute.
 */
	virtual bool is_A_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for AFit attribute.
 */
	virtual bool is_AFit_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for B attribute.
 */
	virtual bool is_B_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for BFit attribute.
 */
	virtual bool is_BFit_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for C attribute.
 */
	virtual bool is_C_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for CFit attribute.
 */
	virtual bool is_CFit_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for Alpha attribute.
 */
	virtual bool is_Alpha_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for AlphaFit attribute.
 */
	virtual bool is_AlphaFit_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for Beta attribute.
 */
	virtual bool is_Beta_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for BetaFit attribute.
 */
	virtual bool is_BetaFit_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for Gamma attribute.
 */
	virtual bool is_Gamma_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for GammaFit attribute.
 */
	virtual bool is_GammaFit_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for Ux attribute.
 */
	virtual bool is_Ux_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for UxFit attribute.
 */
	virtual bool is_UxFit_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for Uy attribute.
 */
	virtual bool is_Uy_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for UyFit attribute.
 */
	virtual bool is_UyFit_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for Uz attribute.
 */
	virtual bool is_Uz_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for UzFit attribute.
 */
	virtual bool is_UzFit_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for AStar attribute.
 */
	virtual bool is_AStar_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for BStar attribute.
 */
	virtual bool is_BStar_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for CStar attribute.
 */
	virtual bool is_CStar_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for AlphaStar attribute.
 */
	virtual bool is_AlphaStar_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for BetaStar attribute.
 */
	virtual bool is_BetaStar_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for GammaStar attribute.
 */
	virtual bool is_GammaStar_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for Simulated attribute.
 */
	virtual bool is_Simulated_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for CrystalNames attribute.
 */
	virtual bool is_CrystalNames_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for AxesNames attribute.
 */
	virtual bool is_AxesNames_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for pseudoAxesProxies attribute.
 */
	virtual bool is_pseudoAxesProxies_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for Angles attribute.
 */
	virtual bool is_Angles_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for AnglesDegenerated attribute.
 */
	virtual bool is_AnglesDegenerated_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for AnglesRangeCheck attribute.
 */
	virtual bool is_AnglesRangeCheck_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for Reflections attribute.
 */
	virtual bool is_Reflections_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for ReflectionsAngles attribute.
 */
	virtual bool is_ReflectionsAngles_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for UB attribute.
 */
	virtual bool is_UB_allowed(Tango::AttReqType type);
/**
 *	Execution allowed for Abort command.
 */
	virtual bool is_Abort_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for AddNewCrystal command.
 */
	virtual bool is_AddNewCrystal_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for AddReflection command.
 */
	virtual bool is_AddReflection_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for AffineCrystal command.
 */
	virtual bool is_AffineCrystal_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for ComputeU command.
 */
	virtual bool is_ComputeU_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for ConfigureCrystal command.
 */
	virtual bool is_ConfigureCrystal_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for CopyCurrentCrystalAs command.
 */
	virtual bool is_CopyCurrentCrystalAs_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for CopyReflectionTo command.
 */
	virtual bool is_CopyReflectionTo_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for DeleteCurrentCrystal command.
 */
	virtual bool is_DeleteCurrentCrystal_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for GetCrystalParameterValues command.
 */
	virtual bool is_GetCrystalParameterValues_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for Load command.
 */
	virtual bool is_Load_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for RemoveReflection command.
 */
	virtual bool is_RemoveReflection_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for Save command.
 */
	virtual bool is_Save_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for SetCrystalParameterValues command.
 */
	virtual bool is_SetCrystalParameterValues_allowed(const CORBA::Any &any);
/**
 * This command gets the device state (stored in its <i>device_state</i> data member) and returns it to the caller.
 *	@return	State Code
 *	@exception DevFailed
 */
	virtual Tango::DevState	dev_state();
/**
 * This command gets the device status (stored in its <i>device_status</i> data member) and returns it to the caller.
 *	@return	Status description
 *	@exception DevFailed
 */
	virtual Tango::ConstDevString	dev_status();
/**
 * Stops the movement of all axis driven by the Diffractometer
 *	@exception DevFailed
 */
	void	abort();
/**
 * Create a new crystal
 *	@param	argin	Name of the new crystal
 *	@exception DevFailed
 */
	void	add_new_crystal(Tango::DevString);
/**
 * This commands requires the definition of 1 reflection defined by (h,k,l ) coordinates.
 *	This coodinates are associated with the current angles configuration.
 *	
 *	TODO: d�taill� le commentaire
 *	reflections i.e : h, k, l, relevance, enable/disable
 *	@param	argin	reflections i.e :  h, k, l, relevance, enable/disable
 *	@exception DevFailed
 */
	void	add_reflection(const Tango::DevVarDoubleArray *);
/**
 * Compute affinement for the current crystal with available list of reflection.
 *	@param	argin	name of the crystal to fit
 *	@return	the fitness of the crystal after the affinement.
 *	@exception DevFailed
 */
	Tango::DevDouble	affine_crystal(Tango::DevString);
/**
 * Compute Matrix U from two reflections
 *	@param	argin	a two elements array with the index of reflection to use
 *	@exception DevFailed
 */
	void	compute_u(const Tango::DevVarLongArray *);
/**
 * Defines crystal lattice parameters.
 *	This information is mandatory for angles calculations.
 *	Angles units are degrees
 *	@param	argin	Crystal parameters : alpha,beta,gamma, A,B,C
 *	@exception DevFailed
 */
	void	configure_crystal(const Tango::DevVarDoubleArray *);
/**
 * Copy the current crytal as another name
 *	@param	argin	
 *	@exception DevFailed
 */
	void	copy_current_crystal_as(Tango::DevString);
/**
 * Copy one reflection from the current crystal to another crystal
 *	@param	argin	Double: Reflection number of the current crystal String : Name of the crystal where copy this reflection
 *	@exception DevFailed
 */
	void	copy_reflection_to(const Tango::DevVarDoubleStringArray *);
/**
 * Delete the current from this device
 *	@exception DevFailed
 */
	void	delete_current_crystal();
/**
 * Return all values of a parameter for the current crystal
 *	@param	argin	Name of parameter (see getParametersNames for the complete list of parameters)
 *	@return	In the order : minimum value, maximum value, affinement enable
 *	@exception DevFailed
 */
	Tango::DevVarDoubleStringArray	*get_crystal_parameter_values(Tango::DevString);
/**
 * Load all crystals
 *	@exception DevFailed
 */
	void	load();
/**
 * This commands removes reflection from the current cristal.
 *	@param	argin	index of reflection to remove
 *	@exception DevFailed
 */
	void	remove_reflection(Tango::DevShort);
/**
 * Save all crystals
 *	@exception DevFailed
 */
	void	save();
/**
 * Double : min value, max value, affinement enable
 *	@param	argin	String : Name of parameter ; Double : min value, max value, affinement enable
 *	@exception DevFailed
 */
	void	set_crystal_parameter_values(const Tango::DevVarDoubleStringArray *);

/**
 *	Read the device properties from database
 */
	 void get_device_property();
//@}

	//	Here is the end of the automatic code generation part
	//-------------------------------------------------------------	



protected :	
	//	Add your own data members here
	//-----------------------------------------
	 bool _helper_not_initialized;

	 TangoHKLAdapter * _hklAdapter;
	 HklGeometryType _type;

	 Tango::DevVarStringArray _crystal_names_dev_varstring_array;

	 DiffractometerConfig _config;

	// thoses methods are declared in the DiffractometerExtra.cpp

	 // refresh crystal parameters
	 void refresh_crystal_parameters(void);

	 bool get_type_from_property(std::string const & name, HklGeometryType &type);

	 void remove_dynamic_attributes(void);

public:

	 void create_dynamic_attributes(void);
};

}	// namespace_ns

#endif	// _DIFFRACTOMETER_H
