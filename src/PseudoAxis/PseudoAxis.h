#ifndef _PSEUDOAXIS_H
#define _PSEUDOAXIS_H

#include <tango.h>

#include "TangoHKLAdapter.h"

//forward declaration
//namespace hkl { class PseudoAxe; }

//	Add your own constants definitions here.
//-----------------------------------------------
namespace PseudoAxis_ns
{

	/**
	 * Class Description:
	 * 
 */

/*
 *	Device States Description:
*  Tango::OFF :      Communication broken with the motion controller
*  Tango::STANDBY :  Axis OK, waiting for commands
*  Tango::MOVING :   Axis is moving
*  Tango::FAULT :    Failure on the axis. Can be hardware failure .
*  Tango::ALARM :    Alarm on the axis. can be soft or hard limit, abort input...
*  Tango::DISABLE :  For debug purpose
 */


	class PseudoAxis: public Tango::Device_4Impl
	{
		public :
			//	Add your own data members here
			//-----------------------------------------


			//	Here is the Start of the automatic code generation part
			//-------------------------------------------------------------	
			/**
			 *	@name attributes
			 *	Attributs member data.
			 */
			//@{
		Tango::DevDouble	*attr_position_read;
		Tango::DevDouble	attr_position_write;
		Tango::DevDouble	*attr_offset_read;
		Tango::DevDouble	attr_offset_write;
		Tango::DevBoolean	*attr_initialized_read;
		Tango::DevBoolean	attr_initialized_write;
		Tango::DevDouble	attr_relativeMove_write;
		Tango::DevString	*attr_mode_read;
		Tango::DevString	attr_mode_write;
		Tango::DevBoolean	*attr_IsInitialised_read;
		Tango::DevBoolean	attr_IsInitialised_write;
		Tango::DevString	*attr_modeNames_read;
//@}

			/**
			 *	@name Device properties
			 *	Device properties member data.
			 */
			//@{
/**
 *	This is the tango url to the device Diffractometer.\nN.B : The instance of the diffractometer must be the same that this PseudoAxis
 */
	string	diffractometerProxy;
/**
 *	This name must be register in the HKL library.
 *	
 *	i.e :
 *	For Eulerian 4 circles : psi, q, q2th, th2th
 */
	string	pseudoAxisName;
//@}

			/**@name Constructors
			 * Miscellaneous constructors */
			//@{
			/**
			 * Constructs a newly allocated Command object.
			 *
			 *	@param cl	Class.
			 *	@param s 	Device Name
			 */
			PseudoAxis(Tango::DeviceClass *cl,string &s);
			/**
			 * Constructs a newly allocated Command object.
			 *
			 *	@param cl	Class.
			 *	@param s 	Device Name
			 */
			PseudoAxis(Tango::DeviceClass *cl,const char *s);
			/**
			 * Constructs a newly allocated Command object.
			 *
			 *	@param cl	Class.
			 *	@param s 	Device name
			 *	@param d	Device description.
			 */
			PseudoAxis(Tango::DeviceClass *cl,const char *s,const char *d);
			//@}

			/**@name Destructor
			 * Only one desctructor is defined for this class */
			//@{
			/**
			 * The object desctructor.
			 */	
			~PseudoAxis() {delete_device();};
			/**
			 *	will be called at device destruction or at init command.
			 */
			void delete_device();
			//@}


			/**@name Miscellaneous methods */
			//@{
			/**
			 *	Initialize the device
			 */
			virtual void init_device();
			/**
			 *	Always executed method befor execution command method.
			 */
			virtual void always_executed_hook();

//@}

/**
 * @name PseudoAxis methods prototypes
 */

//@{
/**
 *	Hardware acquisition for attributes.
 */
	virtual void read_attr_hardware(vector<long> &attr_list);
/**
 *	Extract real attribute values for position acquisition result.
 */
	virtual void read_position(Tango::Attribute &attr);
/**
 *	Write position attribute values to hardware.
 */
	virtual void write_position(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for offset acquisition result.
 */
	virtual void read_offset(Tango::Attribute &attr);
/**
 *	Write offset attribute values to hardware.
 */
	virtual void write_offset(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for initialized acquisition result.
 */
	virtual void read_initialized(Tango::Attribute &attr);
/**
 *	Write initialized attribute values to hardware.
 */
	virtual void write_initialized(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for relativeMove acquisition result.
 */
	virtual void read_relativeMove(Tango::Attribute &attr);
/**
 *	Write relativeMove attribute values to hardware.
 */
	virtual void write_relativeMove(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for mode acquisition result.
 */
	virtual void read_mode(Tango::Attribute &attr);
/**
 *	Write mode attribute values to hardware.
 */
	virtual void write_mode(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for IsInitialised acquisition result.
 */
	virtual void read_IsInitialised(Tango::Attribute &attr);
/**
 *	Write IsInitialised attribute values to hardware.
 */
	virtual void write_IsInitialised(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for modeNames acquisition result.
 */
	virtual void read_modeNames(Tango::Attribute &attr);
/**
 *	Read/Write allowed for position attribute.
 */
	virtual bool is_position_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for offset attribute.
 */
	virtual bool is_offset_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for initialized attribute.
 */
	virtual bool is_initialized_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for relativeMove attribute.
 */
	virtual bool is_relativeMove_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for mode attribute.
 */
	virtual bool is_mode_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for IsInitialised attribute.
 */
	virtual bool is_IsInitialised_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for modeNames attribute.
 */
	virtual bool is_modeNames_allowed(Tango::AttReqType type);
/**
 *	Execution allowed for Stop command.
 */
	virtual bool is_Stop_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for On command.
 */
	virtual bool is_On_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for MotorOFF command.
 */
	virtual bool is_MotorOFF_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for MotorON command.
 */
	virtual bool is_MotorON_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for MotorInit command.
 */
	virtual bool is_MotorInit_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for ComputeNewOffset command.
 */
	virtual bool is_ComputeNewOffset_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for Backward command.
 */
	virtual bool is_Backward_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for Forward command.
 */
	virtual bool is_Forward_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for DefinePosition command.
 */
	virtual bool is_DefinePosition_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for InitializeReferencePosition command.
 */
	virtual bool is_InitializeReferencePosition_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for GetModeParameters command.
 */
	virtual bool is_GetModeParameters_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for SetModeParameters command.
 */
	virtual bool is_SetModeParameters_allowed(const CORBA::Any &any);
/**
 * This command gets the device state (stored in its <i>device_state</i> data member) and returns it to the caller.
 *	@return	State Code
 *	@exception DevFailed
 */
	virtual Tango::DevState	dev_state();
/**
 * This command gets the device status (stored in its <i>device_status</i> data member) and returns it to the caller.
 *	@return	Status description
 *	@exception DevFailed
 */
	virtual Tango::ConstDevString	dev_status();
/**
 * Stop the pseudoAxes and all related axes.
 *	@exception DevFailed
 */
	void	stop();
/**
 * Activate the pseudoAxis and all related motors.
 *	@exception DevFailed
 */
	void	on();
/**
 * DEPRECATED
 *	@exception DevFailed
 */
	void	motor_off();
/**
 * DEPRECATED
 *	@exception DevFailed
 */
	void	motor_on();
/**
 * DEPRECATED
 *	@exception DevFailed
 */
	void	motor_init();
/**
 * The so smart program computes the offset to goal the user position.
 *	@param	argin	The new user position
 *	@exception DevFailed
 */
	void	compute_new_offset(Tango::DevDouble);
/**
 * dummy command for V2 interface
 *	@exception DevFailed
 */
	void	backward();
/**
 * dummy command for V2 interface
 *	@exception DevFailed
 */
	void	forward();
/**
 * dummy command for V2 interface
 *	@param	argin	
 *	@exception DevFailed
 */
	void	define_position(Tango::DevDouble);
/**
 * dummy command for V2 interface
 *	@exception DevFailed
 */
	void	initialize_reference_position();
/**
 * this command return for the current mode, its parameters.
 *	@return	parameters names and values
 *	@exception DevFailed
 */
	Tango::DevVarDoubleStringArray	*get_mode_parameters();
/**
 * 
 *	@param	argin	The parameters to set
 *	@exception DevFailed
 */
	void	set_mode_parameters(const Tango::DevVarDoubleStringArray *);

/**
 *	Read the device properties from database
 */
	 void get_device_property();
//@}

			//	Here is the end of the automatic code generation part
			//-------------------------------------------------------------	
			void set_hkl_library(Diffractometer_ns::TangoHKLAdapter* hkl_library);


		protected :	
			//	Add your own data members here
			//-----------------------------------------
			Diffractometer_ns::PseudoAxisAdapter * _buffer;
			Diffractometer_ns::TangoHKLAdapter * _hklAdapter;
			Diffractometer_ns::PseudoAxisConfig _config;
	};

}	// namespace_ns

#endif	// _PSEUDOAXIS_H
