//===================================================================
//
//	The following table gives the correspondence
//	between commands and method name.
//
//  Command name                 |  Method name
//	----------------------------------------
//  State                        |  dev_state()
//  Status                       |  dev_status()
//  Stop                         |  stop()
//  On                           |  on()
//  MotorOFF                     |  motor_off()
//  MotorON                      |  motor_on()
//  MotorInit                    |  motor_init()
//  ComputeNewOffset             |  compute_new_offset()
//  Backward                     |  backward()
//  Forward                      |  forward()
//  DefinePosition               |  define_position()
//  InitializeReferencePosition  |  initialize_reference_position()
//  GetModeParameters            |  get_mode_parameters()
//  SetModeParameters            |  set_mode_parameters()
//
//===================================================================

#if defined (WIN32)
#include <PseudoAxis.h>
#include <PseudoAxisClass.h>
#include <PogoHelper.h>
#include <tango.h>
#include <TangoExceptionsHelper.h>
#else
#include <tango.h>
#include <TangoExceptionsHelper.h>
#include <PseudoAxis.h>
#include <PseudoAxisClass.h>
#include <PogoHelper.h>
#endif

#include <DeviceProxyHelper.h>

#include <hkl/hkl-pseudoaxis.h>

#include "macros.h"
#include "PseudoAxis.h"
#include "PseudoAxisClass.h"
#include "TangoHKLAdapterFactory.h"

namespace PseudoAxis_ns
{

	//+----------------------------------------------------------------------------
	//
	// method : 		PseudoAxis::PseudoAxis(string &s)
	// 
	// description : 	constructor for simulated PseudoAxis
	//
	// in : - cl : Pointer to the DeviceClass object
	//      - s : Device name 
	//
	//-----------------------------------------------------------------------------
	PseudoAxis::PseudoAxis(Tango::DeviceClass *cl,string &s)
		:Tango::Device_4Impl(cl,s.c_str())
		{
			init_device();
		}

	PseudoAxis::PseudoAxis(Tango::DeviceClass *cl,const char *s)
		:Tango::Device_4Impl(cl,s)
		{
			init_device();
		}

	PseudoAxis::PseudoAxis(Tango::DeviceClass *cl,const char *s,const char *d)
		:Tango::Device_4Impl(cl,s,d)
		{
			init_device();
		}
	//+----------------------------------------------------------------------------
	//
	// method : 		PseudoAxis::delete_device()
	// 
	// description : 	will be called at device destruction or at init command.
	//
	//-----------------------------------------------------------------------------
	void PseudoAxis::delete_device()
	{
		DEBUG_STREAM << "PseudoAxis::delete_device() entering... " << std::endl;

		// no need to release the memory this is done by ~TangoHKLAdapter()
		_buffer = NULL;
		_hklAdapter = NULL;
	}

	//+----------------------------------------------------------------------------
	//
	// method : 		PseudoAxis::init_device()
	// 
	// description : 	will be called at device initialization.
	//
	//-----------------------------------------------------------------------------
	void PseudoAxis::init_device()
	{
		DEBUG_STREAM << "PseudoAxis::init_device() entering... " << std::endl;

		// Initialise variables to default values
		//--------------------------------------------
		this->get_device_property();

		_buffer = NULL;
		_hklAdapter = NULL;

		// initialize the config
		_config.mode = "";
		_config.initialized = false;
		_config.state = Tango::FAULT;
		_config.status = "Not yet initialized";
	}

	//+----------------------------------------------------------------------------
	//
	// method : 		PseudoAxis::get_device_property()
	// 
	// description : 	Read the device properties from database.
	//
	//-----------------------------------------------------------------------------
	void PseudoAxis::get_device_property()
	{
		DEBUG_STREAM << "PseudoAxis::get_device_property() entering... " << std::endl;

		//	Initialize your default values here (if not done with  POGO).
		//------------------------------------------------------------------

		//	Read device properties from database.(Automatic code generation)
		//------------------------------------------------------------------
	Tango::DbData	dev_prop;
	dev_prop.push_back(Tango::DbDatum("DiffractometerProxy"));
	dev_prop.push_back(Tango::DbDatum("PseudoAxisName"));

	//	Call database and extract values
	//--------------------------------------------
	if (Tango::Util::instance()->_UseDb==true)
		get_db_device()->get_property(dev_prop);
	Tango::DbDatum	def_prop, cl_prop;
	PseudoAxisClass	*ds_class =
		(static_cast<PseudoAxisClass *>(get_device_class()));
	int	i = -1;

	//	Try to initialize DiffractometerProxy from class property
	cl_prop = ds_class->get_class_property(dev_prop[++i].name);
	if (cl_prop.is_empty()==false)	cl_prop  >>  diffractometerProxy;
	else {
		//	Try to initialize DiffractometerProxy from default device value
		def_prop = ds_class->get_default_device_property(dev_prop[i].name);
		if (def_prop.is_empty()==false)	def_prop  >>  diffractometerProxy;
	}
	//	And try to extract DiffractometerProxy value from database
	if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  diffractometerProxy;

	//	Try to initialize PseudoAxisName from class property
	cl_prop = ds_class->get_class_property(dev_prop[++i].name);
	if (cl_prop.is_empty()==false)	cl_prop  >>  pseudoAxisName;
	else {
		//	Try to initialize PseudoAxisName from default device value
		def_prop = ds_class->get_default_device_property(dev_prop[i].name);
		if (def_prop.is_empty()==false)	def_prop  >>  pseudoAxisName;
	}
	//	And try to extract PseudoAxisName value from database
	if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  pseudoAxisName;



		//	End of Automatic code generation
		//------------------------------------------------------------------

	}


	//+------------------------------------------------------------------
	/**
	 *	  method: PseudoAxe::dev_state
	 *
	 *	  description:	  method to execute "State"
	 *	  This command gets the device state (stored in its <i>device_state</i> data member) and returns it to the caller.
	 *
	 * @return	  State Code
	 *
	 * TODO to Adapter
	 *
	 */
	//+------------------------------------------------------------------
	Tango::DevState PseudoAxis::dev_state()
	{
		DEBUG_STREAM << "PseudoAxis::dev_state(): entering... !" << endl;

		if (!_hklAdapter || !_buffer)
			this->set_state(Tango::FAULT);
		else {
			this->set_state(_config.state);
			this->set_status(_config.status);
		}
		return this->device_state;
	}

	//+------------------------------------------------------------------
	/**
	 *	  method: PseudoAxe::dev_status
	 *
	 *	  description:	  method to execute "Status"
	 *	  This command gets the device state (stored in its <i>device_state</i> data member) and returns it to the caller.
	 *
	 * @return	  State Code
	 *
	 * TODO to Adapter
	 *
	 */
	//+------------------------------------------------------------------
	Tango::ConstDevString PseudoAxis::dev_status()
	{
		DEBUG_STREAM << "PseudoAxis::dev_status(): entering... !" << endl;

		if (!_hklAdapter) {
			this->device_state = Tango::FAULT;
			if (diffractometerProxy == "")
				this->device_status = "Please set a correct \"diffractometerProxy\" propertie";
			else {
				this->device_status = "The Diffractometer device " +  diffractometerProxy + " is not ready or do not existe!\n";
				this->device_status += "Please start the Diffractometer or check the \"diffractometerProxy\" Propertie";
			}
		} else {
			if(!_buffer) {
				this->device_state = Tango::FAULT;
				if (pseudoAxisName == "")
					this->device_status = "Please set a correct \"pseudoAxisName\" : ";
				else {
					this->device_status = "Cannot find the pseudoAxe \"" + pseudoAxisName + "\" in the " + diffractometerProxy + " Diffractometer device : ";
				}	
				this->device_status += "Set \"pseudoAxisName\" propertie with one of these possible values : ";
				std::vector<std::string> const names = _hklAdapter->pseudo_axis_get_names();
				std::vector<std::string>::const_iterator iter = names.begin();
				std::vector<std::string>::const_iterator end = names.end();
				this->device_status += " " + *iter;
				++iter;
				while(iter != end) {
					this->device_status += ", " + *iter;
					++iter;
				}
			} else {
				this->set_state(_config.state);
				this->set_status(_config.status);
			}
		}
		return this->device_status.c_str();
	}

//+----------------------------------------------------------------------------
//
// method : 		PseudoAxis::always_executed_hook()
// 
// description : 	method always executed before any command is executed
//
//-----------------------------------------------------------------------------
void PseudoAxis::always_executed_hook()
{
	DEBUG_STREAM << "PseudoAxis::always_executed_hook() entering... " << std::endl;
	// here we are looking for the intance of the related Diffractometer to get the right hkladapter instance.
	if(!_hklAdapter)
		_hklAdapter = Diffractometer_ns::TangoHKLAdapterFactory::instance()->get_diffractometer_adapter(diffractometerProxy);

	if(_hklAdapter && !_buffer)
		_buffer = _hklAdapter->pseudo_axis_buffer_new(pseudoAxisName.c_str());
}

//+----------------------------------------------------------------------------
//
// method : 		PseudoAxis::read_attr_hardware
// 
// description : 	Hardware acquisition for attributes.
//
//-----------------------------------------------------------------------------
void PseudoAxis::read_attr_hardware(vector<long> &attr_list)
{
	DEBUG_STREAM << "PseudoAxis::read_attr_hardware(vector<long> &attr_list) entering... "<< endl;
	//      Add your own code here
	if(_buffer)
		_config = _buffer->get_config();
}
//+----------------------------------------------------------------------------
//
// method : 		PseudoAxis::read_initialized
// 
// description : 	Extract real attribute values for initialized acquisition result.
//
//-----------------------------------------------------------------------------
void PseudoAxis::read_initialized(Tango::Attribute &attr)
{
	DEBUG_STREAM << "PseudoAxis::read_initialized(Tango::Attribute &attr) entering... "<< endl;
	//      Add your own code here

	if(_buffer)
		attr.set_value(&_config.initialized);
}

//+----------------------------------------------------------------------------
//
// method : 		PseudoAxis::write_initialized
// 
// description : 	Write initialized attribute values to hardware.
//
//-----------------------------------------------------------------------------
void PseudoAxis::write_initialized(Tango::WAttribute &attr)
{
	DEBUG_STREAM << "PseudoAxis::write_initialized(Tango::WAttribute &attr) entering... "<< endl;
	attr.get_write_value(attr_initialized_write);
	if(_buffer)
		_buffer->set_initialized(attr_initialized_write);

	// need to after the internals before initializing the pseudo
	if (_hklAdapter)
		_hklAdapter->update();
}

//+----------------------------------------------------------------------------
//
// method : 		PseudoAxis::read_mode
// 
// description : 	Extract real attribute values for mode acquisition result.
//
//-----------------------------------------------------------------------------
void PseudoAxis::read_mode(Tango::Attribute &attr)
{
	DEBUG_STREAM << "PseudoAxis::read_mode(Tango::Attribute &attr) entering... "<< endl;

	if(_buffer)
		attr.set_value(&_config.mode);
}

//+----------------------------------------------------------------------------
//
// method : 		PseudoAxis::write_mode
// 
// description : 	Write mode attribute values to hardware.
//
//-----------------------------------------------------------------------------
void PseudoAxis::write_mode(Tango::WAttribute &attr)
{
	DEBUG_STREAM << "PseudoAxis::write_mode(Tango::WAttribute &attr) entering... "<< endl;
	attr.get_write_value(attr_mode_write);
	if(_buffer)
		_buffer->set_mode(attr_mode_write);
}

//+----------------------------------------------------------------------------
//
// method : 		PseudoAxis::read_modeNames
// 
// description : 	Extract real attribute values for modeNames acquisition result.
//
//-----------------------------------------------------------------------------
void PseudoAxis::read_modeNames(Tango::Attribute &attr)
{
	DEBUG_STREAM << "PseudoAxis::read_modeNames(Tango::Attribute &attr) entering... "<< endl;
	if(_buffer){
		Matrix<char *> const & img = _config.mode_names;
		attr.set_value(img.data, img.xdim);
	}
}

//+----------------------------------------------------------------------------
//
// method : 		PseudoAxis::write_IsInitialised
// 
// description : 	Write IsInitialised attribute values to hardware.
//
//-----------------------------------------------------------------------------
void PseudoAxis::write_IsInitialised(Tango::WAttribute &attr)
{
	DEBUG_STREAM << "PseudoAxis::write_IsInitialised(Tango::WAttribute &attr) entering... "<< endl;
	//      Add your own code here
	attr.get_write_value(attr_IsInitialised_write);
	if(_buffer)
		_buffer->set_initialized(attr_IsInitialised_write);

	// need to after the internals before initializing the pseudo
	if (_hklAdapter)
		_hklAdapter->update();
}


//+----------------------------------------------------------------------------
//
// method : 		PseudoAxis::read_position
// 
// description : 	Extract real attribute values for position acquisition result.
//
//-----------------------------------------------------------------------------
void PseudoAxis::read_position(Tango::Attribute &attr)
{
	DEBUG_STREAM << "PseudoAxis::read_position(Tango::Attribute &attr) entering... "<< endl;
	//      Add your own code here

	if(_buffer){
		Tango::WAttribute & attrw = dev_attr->get_w_attr_by_name(attr.get_name().c_str());
		attr.set_value(&_config.position_r);
		attrw.set_write_value(_config.position_w);
	}
}

//+----------------------------------------------------------------------------
//
// method : 		PseudoAxis::write_position
// 
// description : 	Write position attribute values to hardware.
//
//-----------------------------------------------------------------------------
void PseudoAxis::write_position(Tango::WAttribute &attr)
{
	DEBUG_STREAM << "PseudoAxis::write_position(Tango::WAttribute &attr) entering... "<< endl;
	//      Add your own code here

	attr.get_write_value(attr_position_write);
	if(_buffer)
		_buffer->set_position(attr_position_write);
}

//+----------------------------------------------------------------------------
//
// method : 		PseudoAxis::read_offset
// 
// description : 	Extract real attribute values for offset acquisition result.
//
//-----------------------------------------------------------------------------
void PseudoAxis::read_offset(Tango::Attribute &attr)
{
	DEBUG_STREAM << "PseudoAxis::read_offset(Tango::Attribute &attr) entering... "<< endl;
	//      Add your own code here
	if(_buffer)
		attr.set_value(&_config.offset);
}

//+----------------------------------------------------------------------------
//
// method : 		PseudoAxis::write_offset
// 
// description : 	Write offset attribute values to hardware.
//
//-----------------------------------------------------------------------------
void PseudoAxis::write_offset(Tango::WAttribute &attr)
{
	DEBUG_STREAM << "PseudoAxis::write_offset(Tango::WAttribute &attr) entering... "<< endl;
	//      Add your own code here
	attr.get_write_value(attr_offset_write);
	if(_buffer)
		_buffer->set_offset(attr_offset_write);
}

//+----------------------------------------------------------------------------
//
// method : 		PseudoAxis::read_relativeMove
// 
// description : 	Extract real attribute values for relativeMove acquisition result.
//
//-----------------------------------------------------------------------------
void PseudoAxis::read_relativeMove(Tango::Attribute &attr)
{
	DEBUG_STREAM << "PseudoAxis::read_relativeMove(Tango::Attribute &attr) entering... "<< endl;
	//      Add your own code here
	attr.set_value(&attr_relativeMove_write);
}

//+----------------------------------------------------------------------------
//
// method : 		PseudoAxis::write_relativeMove
// 
// description : 	Write relativeMove attribute values to hardware.
//
//-----------------------------------------------------------------------------
void PseudoAxis::write_relativeMove(Tango::WAttribute &attr)
{
	DEBUG_STREAM << "PseudoAxis::write_relativeMove(Tango::WAttribute &attr) entering... "<< endl;
	//      Add your own code here
	attr.get_write_value(attr_relativeMove_write);
	double new_position = _config.position_r + attr_relativeMove_write;

	// Write to the "position" attribute to move the axe.
	Tango::DeviceProxyHelper moi(this->device_name, this);
	moi.write_attribute("position", new_position);
}


//+----------------------------------------------------------------------------
//
// method : 		PseudoAxis::read_IsInitialised
// 
// description : 	Extract real attribute values for IsInitialised acquisition result.
//
//-----------------------------------------------------------------------------
void PseudoAxis::read_IsInitialised(Tango::Attribute &attr)
{
	DEBUG_STREAM << "PseudoAxis::read_IsInitialised(Tango::Attribute &attr) entering... "<< endl;
	//      Add your own code here

	if(_buffer)
		attr.set_value(&_config.initialized);
}

//+------------------------------------------------------------------
/**
 *	method:	PseudoAxis::motor_off
 *
 *	description:	method to execute "MotorOFF"
 *	DEPRECATED
 *
 *
 */
//+------------------------------------------------------------------
void PseudoAxis::motor_off()
{
	DEBUG_STREAM << "PseudoAxis::motor_off(): entering... !" << endl;

	//	Add your own code to control device here
}

//+------------------------------------------------------------------
/**
 *	method:	PseudoAxis::motor_on
 *
 *	description:	method to execute "MotorON"
 *	DEPRECATED
 *
 *
 */
//+------------------------------------------------------------------
void PseudoAxis::motor_on()
{
	DEBUG_STREAM << "PseudoAxis::motor_on(): entering... !" << endl;

	//	Add your own code to control device here
	if(_buffer)
		_buffer->on();
}

//+------------------------------------------------------------------
/**
 *	method:	PseudoAxis::compute_new_offset
 *
 *	description:	method to execute "ComputeNewOffset"
 *	The so smart program computes the offset to goal the user position.
 *
 * @param	argin	The new user position
 *
 */
//+------------------------------------------------------------------
void PseudoAxis::compute_new_offset(Tango::DevDouble argin)
{
	DEBUG_STREAM << "PseudoAxis::compute_new_offset(): entering... !" << endl;

	//	Add your own code to control device here
	if(_buffer){
		Tango::DevDouble offset= _buffer->compute_new_offset(argin);
		Tango::DeviceProxyHelper moi(this->device_name, this);
		moi.write_attribute("offset", offset);
	}
}

//+------------------------------------------------------------------
/**
 *	method:	PseudoAxis::stop
 *
 *	description:	method to execute "Stop"
 *	Stop the pseudoAxes and all related axes.
 *
 *
 */
//+------------------------------------------------------------------
void PseudoAxis::stop()
{
	DEBUG_STREAM << "PseudoAxis::stop(): entering... !" << endl;

	//	Add your own code to control device here
	if(_buffer)
		_buffer->stop();

}

//+------------------------------------------------------------------
/**
 *	method:	PseudoAxis::motor_init
 *
 *	description:	method to execute "MotorInit"
 *	DEPRECATED
 *
 *
 */
//+------------------------------------------------------------------
void PseudoAxis::motor_init()
{
	DEBUG_STREAM << "PseudoAxis::motor_init(): entering... !" << endl;

	// DEPRECATED
	//	Add your own code to control device here
	if (_buffer)
		_buffer->set_initialized(true);
	if(_hklAdapter)
		_hklAdapter->update();
}

//+------------------------------------------------------------------
/**
 *	method:	PseudoAxis::backward
 *
 *	description:	method to execute "Backward"
 *	dummy command for V2 interface
 *
 *
 */
//+------------------------------------------------------------------
void PseudoAxis::backward()
{
	DEBUG_STREAM << "PseudoAxis::backward(): entering... !" << endl;

	//	Add your own code to control device here

}

//+------------------------------------------------------------------
/**
 *	method:	PseudoAxis::forward
 *
 *	description:	method to execute "Forward"
 *	dummy command for V2 interface
 *
 *
 */
//+------------------------------------------------------------------
void PseudoAxis::forward()
{
	DEBUG_STREAM << "PseudoAxis::forward(): entering... !" << endl;

	//	Add your own code to control device here

}

//+------------------------------------------------------------------
/**
 *	method:	PseudoAxis::define_position
 *
 *	description:	method to execute "DefinePosition"
 *	dummy command for V2 interface
 *
 * @param	argin	
 *
 */
//+------------------------------------------------------------------
void PseudoAxis::define_position(Tango::DevDouble argin)
{
	DEBUG_STREAM << "PseudoAxis::define_position(): entering... !" << endl;

	//	Add your own code to control device here

}

//+------------------------------------------------------------------
/**
 *	method:	PseudoAxis::initialize_reference_position
 *
 *	description:	method to execute "InitializeReferencePosition"
 *	dummy command for V2 interface
 *
 *
 */
//+------------------------------------------------------------------
void PseudoAxis::initialize_reference_position()
{
	DEBUG_STREAM << "PseudoAxis::initialize_reference_position(): entering... !" << endl;

	//	Add your own code to control device here

}


//+------------------------------------------------------------------
/**
 *	method:	PseudoAxis::get_mode_parameters
 *
 *	description:	method to execute "GetModeParameters"
 *	this command return for the current mode, its parameters.
 *
 * @return	parameters names and values
 *
 */
//+------------------------------------------------------------------
Tango::DevVarDoubleStringArray *PseudoAxis::get_mode_parameters()
{
	//	POGO has generated a method core with argout allocation.
	//	If you would like to use a static reference without copying,
	//	See "TANGO Device Server Programmer's Manual"
	//		(chapter : Writing a TANGO DS / Exchanging data)
	//------------------------------------------------------------
	Tango::DevVarDoubleStringArray	*argout  = new Tango::DevVarDoubleStringArray();
	argout->dvalue.length(1);
	argout->dvalue[0] = 0.0;
	argout->svalue.length(1);
	argout->svalue[0] = CORBA::string_dup("dummy");
	DEBUG_STREAM << "PseudoAxis::get_mode_parameters(): entering... !" << endl;

	//	Add your own code to control device here
	if(_buffer)
		_buffer->get_mode_parameters(argout);

	return argout;
}

//+------------------------------------------------------------------
/**
 *	method:	PseudoAxis::set_mode_parameters
 *
 *	description:	method to execute "SetModeParameters"
 *
 * @param	argin	The parameters to set
 *
 */
//+------------------------------------------------------------------
void PseudoAxis::set_mode_parameters(const Tango::DevVarDoubleStringArray *argin)
{
	DEBUG_STREAM << "PseudoAxis::set_mode_parameters(): entering... !" << endl;

	//	Add your own code to control device here
	if(_buffer)
		_buffer->set_mode_parameters(argin);

}




//+------------------------------------------------------------------
/**
 *	method:	PseudoAxis::on
 *
 *	description:	method to execute "On"
 *	Activate the pseudoAxis and all related motors.
 *
 *
 */
//+------------------------------------------------------------------
void PseudoAxis::on()
{
	DEBUG_STREAM << "PseudoAxis::on(): entering... !" << endl;

	//	Add your own code to control device here
	if(_buffer)
		_buffer->on();
}

}	//	namespace
