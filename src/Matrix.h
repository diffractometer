#ifndef _MATRIX_H_
#define _MATRIX_H_

template<class T>
struct Matrix
{
	Matrix(void)
		{
			this->xdim = 0;
			this->ydim = 0;
			this->data = NULL;
			this->_own_data = true;
		}

	Matrix(size_t xdim, size_t ydim)
		{
			this->xdim = xdim;
			this->ydim = ydim;
			this->data = new T[xdim * ydim];
			this->_own_data = true;
		}

	Matrix(Matrix const & m)
		{
			this->xdim = m.xdim;
			this->ydim = m.ydim;
			_own_data = m._own_data;
			if(_own_data){
				this->data = new T[this->xdim * this->ydim];
				::memcpy(this->data, m.data, sizeof(*m.data) * this->xdim * this->ydim);
			} else {
				this->data = m.data;
			}
		}

	Matrix & operator= (Matrix const & m)
		{
			this->xdim = m.xdim;
			this->ydim = m.ydim;
			this->_own_data = m._own_data;
			if(this->_own_data){
				this->data = new T[this->xdim * this->ydim];
				::memcpy(this->data, m.data, sizeof(*m.data) * this->xdim * this->ydim);
			} else {
				this->data = m.data;
			}
			return *this;
		}

	virtual ~Matrix(void)
		{
			if(_own_data){
				this->xdim = 0;
				this->ydim = 0;
				if(this->data){
					delete[] this->data;
					this->data = NULL;
				}
			}
		}

	void resize(size_t xdim, size_t ydim)
		{
			if(this->_own_data){
				if(xdim != this->xdim
				   || ydim != this->ydim){
					if(this->data)
						delete[] this->data;
					this->data = new T[xdim * ydim];
					this->xdim = xdim;
					this->ydim = ydim;
				}
			}
		}

	void clear(void)
		{
			if(this->xdim && this->ydim && this->data)
				::memset(this->data, 0, sizeof(T) * this->xdim * this->ydim);
		}

	void attach_to_buffer(T *data, size_t xdim, size_t ydim)
		{
			if(_own_data && this->data)
				delete[] this->data;
			this->xdim = xdim;
			this->ydim = ydim;
			this->data = data;
			this->_own_data = false;
		}

	void attach_to_const_buffer(const T * data, size_t xdim, size_t ydim)
		{
			if(_own_data && this->data)
				delete[] this->data;
			this->xdim = xdim;
			this->ydim = ydim;
			this->data = const_cast<T *>(data);
			this->_own_data = false;
		}

	void set_data_from_buffer(T const *data, size_t xdim, size_t ydim)
		{
			::memcpy(this->data, data,  xdim * ydim * sizeof(T));
		}

public:

	size_t xdim;
	size_t ydim;
	T *data;

private:

	bool _own_data;
};
#endif
