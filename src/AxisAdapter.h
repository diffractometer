#ifndef __AXIS_ADAPTER_H__
#define __AXIS_ADAPTER_H__

#include <tango.h>
#include <hkl/hkl-axis.h>

namespace Diffractometer_ns {

	// forward declaration
	class TangoHKLAdapter;

	class AxisAdapter
	{
		friend class TangoHKLAdapter;

	public:
		AxisAdapter(TangoHKLAdapter * hklAdapter, HklAxis *axis_r, HklAxis *axis_w,
			    HklAxis *axis_r_real, HklAxis *axis_w_real);

		virtual ~AxisAdapter(void);

		std::string const get_name(void) const;
		std::string const get_proxy_name(void) const;

		Tango::DevState const & get_state(void) const {return _state;}
		void set_state(Tango::DevState const & state) {_state = state;}

		bool const is_ready(void) const;

		double const & get_read(void) const {return _read;}

		double const & get_read_real(void) const {return _read_real;}

		double const & get_write(void) const {return _write;}

		double const & get_write_real(void) const {return _write_real;}

		void write(double value);

		void from_HklAxis(void);

		void to_proxy(void);

		void stop(void);

		void on(void);

	private:

		TangoHKLAdapter * _hklAdapter;
		std::string _proxy_name;
		std::string _proxy_stop_command_name;
		std::string _proxy_on_command_name;
		Tango::AttributeProxy *_proxy;
		HklAxis *_axis_r;
		HklAxis *_axis_w;
		HklAxis *_axis_r_real;
		HklAxis *_axis_w_real;
		double _read;
		double _write;
		double _read_real;
		double _write_real;
		double _min;
		double _max;
		Tango::DevState _state; // state of axe

		bool connect(char *proxy_name);

		void to_HklAxis(void);

		void from_proxy(bool simulated);
	}; 
}

#endif// __Axis_Adapter_H__
