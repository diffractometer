#include <iomanip>

#include <hkl/hkl-pseudoaxis.h>

#include "macros.h"
#include "TangoHKLAdapter.h"
#include "Diffractometer.h"
#include "PseudoAxes.h"
#include "AxisAdapter.h"
#include "AxisAttrib.h"
#include "PseudoAxesAttrib.h"

//#define DEBUG
#ifndef NDEBUG
# define STDOUT(stream) std::cout << stream
#else
# define STDOUT(stream) {};
#endif

namespace Diffractometer_ns
{
	//+----------------------------------------------------------------------------
	//
	// method :		  TangoHKLAdapter::TangoHKLAdapter(string &s)
	//
	// description :	  constructor for the adapter between HKl library and Tango
	//
	// in : - type  : Type of diffractometer to instancate
	//
	//-----------------------------------------------------------------------------
	TangoHKLAdapter::TangoHKLAdapter(Diffractometer *device, HklGeometryType type) :
		_device(device),
		_type(type)
	{
		size_t i, len;
		HklAxis *axes_r;
		HklAxis *axes_w;
		HklAxis *axes_r_real;
		HklAxis *axes_w_real;
		const HklGeometryConfig *config;

		this->ready = false;
		_auto_update_from_proxies = false;
		_lambdaAttributeProxy = NULL;
		_wrong_nb_of_axis_proxies = false;
		_ux = 0;
		_uy = 0;
		_uz = 0;
		_uxfit = true;
		_uyfit = true;
		_uzfit = true;
		_angles_idx = 0;

		duration.Start();

		// Create the hkl part
		// 4 geometries
		config = hkl_geometry_factory_get_config_from_type(type);

		_diffractometer = new HklDiffractometer;
		_diffractometer->geometry_r = hkl_geometry_factory_new(config, 50 * HKL_DEGTORAD);
		_diffractometer->geometry_w = hkl_geometry_factory_new(config, 50 * HKL_DEGTORAD);
		_diffractometer->geometry_r_real = hkl_geometry_factory_new(config, 50 * HKL_DEGTORAD);
		_diffractometer->geometry_w_real = hkl_geometry_factory_new(config, 50 * HKL_DEGTORAD);

		// 2 detectors
		_diffractometer->detector = hkl_detector_factory_new(HKL_DETECTOR_TYPE_0D);
		_diffractometer->detector->idx = 1;

		_diffractometer->detector_real = hkl_detector_factory_new(HKL_DETECTOR_TYPE_0D);
		_diffractometer->detector_real->idx = 1;

		// 1 sample list
		_diffractometer->samples = hkl_sample_list_new();

		// the pseudoAxesenginesList (one per geometry)
		_diffractometer->engines_r = hkl_pseudo_axis_engine_list_factory(config);
		_diffractometer->engines_w = hkl_pseudo_axis_engine_list_factory(config);
		_diffractometer->engines_r_real = hkl_pseudo_axis_engine_list_factory(config);
		_diffractometer->engines_w_real = hkl_pseudo_axis_engine_list_factory(config);
		this->update_pseudo_axis_engines();

		// fill the axisAdapter.
		len = HKL_LIST_LEN(_diffractometer->geometry_r->axes);
		axes_r = _diffractometer->geometry_r->axes;
		axes_w = _diffractometer->geometry_w->axes;
		axes_r_real = _diffractometer->geometry_r_real->axes;
		axes_w_real = _diffractometer->geometry_w_real->axes;
		for(i=0; i<len; ++i)
			_axes.push_back(AxisAdapter(this, &axes_r[i], &axes_w[i], &axes_r_real[i], &axes_w_real[i]));

		// hack to connect all axes the first time
		_auto_update_from_proxies = true;
		this->connect_all_proxies();
		_auto_update_from_proxies = false;

		// fill the pseudoAxesAdapters
		{
			omni_mutex_lock lock(_lock);

			len = HKL_LIST_LEN(_diffractometer->engines_r->engines);
			_pseudo_axes_proxies.resize(len, 1);
			for(i=0; i<len; ++i){
				PseudoAxesAdapter *adapter;
				HklPseudoAxisEngine *engine_r = _diffractometer->engines_r->engines[i];
				HklPseudoAxisEngine *engine_w = _diffractometer->engines_w->engines[i];
				adapter = new PseudoAxesAdapter(this, i, engine_r, engine_w);
				_pseudoAxesAdapters.push_back(adapter);
				_pseudo_axes_proxies.data[i] = const_cast<char *>(adapter->_proxy_name.c_str());
			}
		}

		// create the dynamic attributes
		this->create_axes_dynamic_attributes();

		// set the default lambda
		this->set_lambda(1.54);
	}

	TangoHKLAdapter::~TangoHKLAdapter(void)
	{
		unsigned int i;

		this->destroy_axes_dynamic_attributes();

		// remove all pseudo axis adapters
		for(i=0;i<_pseudoAxisAdapters.size();i++)
			delete _pseudoAxisAdapters[i];

		// remove all pseudo axes adapters;
		for(i=0; i<_pseudoAxesAdapters.size(); ++i)
			delete _pseudoAxesAdapters[i];

		// remove all axisAdapter
		_axes.clear();

		if (_lambdaAttributeProxy)
			delete _lambdaAttributeProxy;

		// remove the hkl part
		hkl_geometry_free(_diffractometer->geometry_r);
		hkl_geometry_free(_diffractometer->geometry_w);
		hkl_geometry_free(_diffractometer->geometry_r_real);
		hkl_geometry_free(_diffractometer->geometry_w_real);
		hkl_detector_free(_diffractometer->detector);
		hkl_detector_free(_diffractometer->detector_real);
		hkl_sample_list_free(_diffractometer->samples);
		hkl_pseudo_axis_engine_list_free(_diffractometer->engines_r);
		hkl_pseudo_axis_engine_list_free(_diffractometer->engines_w);
		hkl_pseudo_axis_engine_list_free(_diffractometer->engines_r_real);
		hkl_pseudo_axis_engine_list_free(_diffractometer->engines_w_real);

		if(_diffractometer) {
			delete _diffractometer;
			_diffractometer = NULL; 
		}
	}

	void TangoHKLAdapter::connect_all_proxies(void)
	{
		omni_mutex_lock lock(_lock);

		// connect the lambda proxy
		if ((!_lambdaAttributeProxy && _auto_update_from_proxies)
		    && _device->lambdaAttributeProxy != "") {
			try
			{
				_lambdaAttributeProxy = new Tango::AttributeProxy(_device->lambdaAttributeProxy);
			}
			catch(...) {
			}
		}

		// connect the axes proxies
		if (!this->ready && _auto_update_from_proxies) {
			unsigned int nb;

			nb = _device->realAxisProxies.size();
			if (nb != _axes.size()) {
				_wrong_nb_of_axis_proxies = true;
				return;
			} else {
				unsigned int i, j;
				bool ready = true;

				for(i=0; i<nb; ++i) {
					AxisAdapter & axis = _axes[i];
					if (!axis.is_ready()){
						// Find axis in the proxy list
						for(j=0; j<nb; ++j) {
							char *line = strdup(_device->realAxisProxies[j].c_str());
							char *last;
							char *axis_name = strtok_r(line, ":", &last);
							char *proxy_name = strtok_r(NULL, ":", &last);
							if (axis.get_name() == axis_name)
								axis.connect(proxy_name);
							free(line);
						}
						if (!axis.is_ready())
							ready = false;
					}
				}
				this->ready = ready;
			}
		}
	}

	void TangoHKLAdapter::set_lambda(double lambda)
	{
		omni_mutex_lock lock(_lock);

		if ((!_auto_update_from_proxies || _device->lambdaAttributeProxy == "")
		    && lambda > 0){
			_lambda = lambda;
			_diffractometer->geometry_r->source.wave_length = lambda;
			_diffractometer->geometry_w->source.wave_length = lambda;
			_diffractometer->geometry_r_real->source.wave_length = lambda;
			_diffractometer->geometry_w_real->source.wave_length = lambda;
		}
	}

	// this method connect engines to the right geometry, detector and sample
	void TangoHKLAdapter::update_pseudo_axis_engines(void)
	{
		hkl_pseudo_axis_engine_list_init(_diffractometer->engines_r,
						 _diffractometer->geometry_r,
						 _diffractometer->detector,
						 _diffractometer->samples->current);

		hkl_pseudo_axis_engine_list_init(_diffractometer->engines_w,
						 _diffractometer->geometry_w,
						 _diffractometer->detector,
						 _diffractometer->samples->current);

		hkl_pseudo_axis_engine_list_init(_diffractometer->engines_r_real,
						 _diffractometer->geometry_r_real,
						 _diffractometer->detector,
						 _diffractometer->samples->current);

		hkl_pseudo_axis_engine_list_init(_diffractometer->engines_w_real,
						 _diffractometer->geometry_w_real,
						 _diffractometer->detector,
						 _diffractometer->samples->current);
	}

	void TangoHKLAdapter::update_lambda(void)
	{
		if (_lambdaAttributeProxy && _auto_update_from_proxies){
			try{
				_lambdaAttributeProxy->read() >> _lambda;
				_diffractometer->geometry_r->source.wave_length = _lambda;
				_diffractometer->geometry_w->source.wave_length = _lambda;
				_diffractometer->geometry_r_real->source.wave_length = _lambda;
				_diffractometer->geometry_w_real->source.wave_length = _lambda;
			}
			catch (Tango::DevFailed &){
			}
		}else
			_lambda = _diffractometer->geometry_r->source.wave_length;
	}

	/**
	 * this method update the angles attribut from the engines->geometry list
	 */
	void TangoHKLAdapter::update_angles(void)
	{
		size_t i, j;
		size_t xdim;
		size_t ydim;
		HklPseudoAxisEngine *engine;
		double *data;
		HklGeometryListItem **items;

		if(!_auto_update_from_proxies)
			engine = hkl_pseudo_axis_engine_list_get_by_name(_diffractometer->engines_w, "hkl");
		else
			engine = hkl_pseudo_axis_engine_list_get_by_name(_diffractometer->engines_w_real, "hkl");

		// update the computed_angles part
		xdim = 1 + HKL_LIST_LEN(_diffractometer->geometry_r->axes);
		ydim = hkl_geometry_list_len(engine->engines->geometries);

		_angles.resize(xdim, ydim);
		_angles.clear();

		//fill the array
		data = _angles.data;
		items = engine->engines->geometries->items;
		for(j=0; j<ydim; ++j){
			HklGeometry *geom = items[j]->geometry;

			data[0] = j;
			for(i=1; i<xdim; ++i)
				data[i] = hkl_axis_get_value_unit(&geom->axes[i-1]);
			data += xdim;
		}
	}

	/**
	 * this method update the reflections_angles when we change a sample parameter
	 */
	void TangoHKLAdapter::update_reflections_angles(void)
	{
		HklSample *sample = _diffractometer->samples->current;
		if (sample) {
			unsigned int i, j;
			size_t rdim;
			double *data;

			// the reflection Angles
			rdim = HKL_LIST_LEN(sample->reflections);
			_reflections_angles.resize(rdim, rdim);
			_reflections_angles.clear();

			data = _reflections_angles.data;
			for(i=0; i<rdim; ++i) {
				for(j=0; j<rdim; ++j) {
					double angle = 0.;	
					if (j < i)
						angle = hkl_sample_get_reflection_theoretical_angle(sample, i, j);
					else if (j > i)
						angle = hkl_sample_get_reflection_mesured_angle(sample, i, j);

					*data = angle * HKL_RADTODEG;
					data++;
				}
			}
		}
	}

	void TangoHKLAdapter::update_reflections(void)
	{
		HklSample *sample = _diffractometer->samples->current;
		if (sample) {

			// Allocation of the image
			unsigned int xdim = 6 + HKL_LIST_LEN(_diffractometer->geometry_r->axes);
			unsigned int ydim = HKL_LIST_LEN(sample->reflections);
			_reflections.resize(xdim, ydim);
			
			size_t i = 0;
			double *data = _reflections.data;
			for(i=0; i<ydim; ++i) {
				HklSampleReflection *r;

				r = hkl_sample_get_ith_reflection(sample, i);
				if (r) {
					size_t k;
					HklAxis *axes = r->geometry->axes;

					data[0] = i;
					data[1] = r->hkl.data[0];
					data[2] = r->hkl.data[1];
					data[3] = r->hkl.data[2];
					data[4] = 0;
					data[5] = (double)r->flag;

					for(k=0; k<HKL_LIST_LEN(r->geometry->axes); ++k)
						data[6 + k] = hkl_axis_get_value_unit(&axes[k]);
				}
				data += xdim;
			}	
		}
	}

	void TangoHKLAdapter::update_ub(void)
	{
		HklSample const *sample = _diffractometer->samples->current;
		if(sample){
			size_t dim = 3;
			_ub.resize(dim, dim);
			_ub.set_data_from_buffer(&(sample->UB.data[0][0]), dim, dim);
		}
	}

	void TangoHKLAdapter::update_ux_uy_uz(void)
	{
		HklSample *sample = _diffractometer->samples->current;

		if(sample){
			hkl_matrix_to_euler(&sample->U, &_ux, &_uy, &_uz);
			_ux *= HKL_RADTODEG;
			_uy *= HKL_RADTODEG;
			_uz *= HKL_RADTODEG;
		}
	}

	/**
	 * this method update all the AxisAdapter from the proxy every 200 ms.
	 * this from_proxy get the real part from the proxy and the "sim" part
	 * from the HklAxis in simulated mode or from the proxy in real mode
	 * else it updates them from the HklAxis.
	 *
	 * every 200 ms
	 *   simulated:
	 *     real <- proxy
	 *     sim <- HklAxis
	 *   non simulated:
	 *     real <- proxy
	 *     sim <- proxy
	 * rest of the time
	 *   real <- HklAxis
	 *   simulated -> HklAxis
	 */
	void TangoHKLAdapter::update_axis_adapters(void)
	{
		size_t i;

		// first read from the proxy.
		duration.Stop();
		if(duration.GetDurationInMs() >= 200) {
			for(size_t i=0; i<_axes.size(); ++i)
				_axes[i].from_proxy(!_auto_update_from_proxies);
			duration.Start();
		}else
			for(i=0; i<_axes.size(); ++i)
				_axes[i].from_HklAxis();
	}

	void TangoHKLAdapter::update_hkl_from_axis_adapters(void)
	{
		size_t i;

		// set the axis
		for(i=0; i<_axes.size(); ++i)
			_axes[i].to_HklAxis();

		// update the pseudo axes
		if(_diffractometer && _diffractometer->samples){
			hkl_pseudo_axis_engine_list_get(_diffractometer->engines_r);
			hkl_pseudo_axis_engine_list_get(_diffractometer->engines_w);
			hkl_pseudo_axis_engine_list_get(_diffractometer->engines_r_real);
			hkl_pseudo_axis_engine_list_get(_diffractometer->engines_w_real);
		}
	}

	void TangoHKLAdapter::update_pseudo_axis_adapters_from_hkl(void)
	{
		for(size_t i=0;i<_pseudoAxisAdapters.size();++i)
			_pseudoAxisAdapters[i]->update();
	}

	void TangoHKLAdapter::update_proxies_from_axis_adapters(void)
	{
		size_t i;

		// first stop motion of all axes.
		for(i=0; i<_axes.size(); ++i)
			_axes[i].stop();

		// then send write values to the proxies
		for(i=0; i<_axes.size(); ++i)
			_axes[i].to_proxy();
	}

	void TangoHKLAdapter::update_proxies_from_pseudo_axis_adapters(PseudoAxisAdapter *adapter)
	{
		adapter->to_proxies();
	}

	void TangoHKLAdapter::update_pseudo_axes_adapters_from_hkl(void)
	{
		for(size_t i=0; i<_pseudoAxesAdapters.size(); ++i)
			_pseudoAxesAdapters[i]->update();
	}

	void TangoHKLAdapter::update_state_and_status(void)
	{
		Tango::DevState state;
		std::string status;
		std::string extra_status;

		state = Tango::STANDBY;
		if (!_auto_update_from_proxies)
			status = "AutoUpdateFromProxies OFF";
		else {
			status = "AutoUpdateFromProxies ON";

			if (!this->ready) {
				state = Tango::FAULT;
				extra_status += "\nCan not connect to axes proxies";
				// update the monochromator proxy status.
				if (!_lambdaAttributeProxy && _auto_update_from_proxies) {
					extra_status += "\nCan not connect to the lambdaAttributeProxy";
					extra_status += "\nCheck also the lambdaAttributeProxy property";
				}
			} else {
				try {
					for(unsigned int i=0; i<_axes.size(); ++i) {
						AxisAdapter const & axis = _axes[i];
						std::string proxy_name = axis.get_proxy_name();
						Tango::DevState tmpState = axis.get_state();

						::compose_state(state, tmpState);
						if (tmpState != Tango::STANDBY)
							extra_status += "\n" + proxy_name + " is in " + Tango::DevStateName[tmpState];
					}
				} catch(...)
				{
					state = Tango::FAULT;
					extra_status += "\nCan not connect to axes proxies";
				}

			}
		}

		if (_diffractometer){
			status += "\nSample: ";
			if(!_diffractometer->samples->current){
				status += "Not yet Set";
				state = Tango::FAULT;
			} else
				status += _diffractometer->samples->current->name;
			if (state == Tango::STANDBY)
				extra_status += "\nready to compute hkl";
		}else{
			state = Tango::FAULT;
			extra_status += "\nhkl core not yet initialized !!!";
		}
		status += "\nDiffractometer status: ";
		status += Tango::DevStateName[state];
		status += extra_status;

		_diffractometerConfig.state = state;
		_diffractometerConfig.status = status;
	}

	void TangoHKLAdapter::update(void)
	{
		/**********************************************
		 *      CRITICAL SECTION
		 **********************************************/
		omni_mutex_lock lock(_lock);

		this->update_lambda();
		this->update_axis_adapters();
		this->update_hkl_from_axis_adapters();
		this->update_pseudo_axis_adapters_from_hkl();
		this->update_pseudo_axes_adapters_from_hkl();
		this->update_state_and_status();
		/**********************************************
		 *    END OF CRITICAL SECTION
		 **********************************************/
	}  

	/********************/	
	/* State and status */
	/********************/
	void TangoHKLAdapter::get_diffractometer_config(DiffractometerConfig & config)
	{
		omni_mutex_lock lock(_lock);

		config = _diffractometerConfig;
	}

	/************/
	/* hkl part */
	/************/

	short & TangoHKLAdapter::get_angles_idx(void)
	{
		omni_mutex_lock lock(_lock);

		return _angles_idx;
	}

	void TangoHKLAdapter::set_angles_idx(short idx)
	{
		omni_mutex_lock lock(_lock);

		// A REVOIR
		if(idx >= 0 && idx < (int)_angles.ydim){
			size_t i;
			double *values;

			_angles_idx = idx;
			values = &_angles.data[1 + idx * _angles.xdim];
			for(i=0; i<_axes.size(); ++i)
				this->write_axis_i(_axes[i], values[i]);
		}
	}

	void TangoHKLAdapter::load(void)
	{
		this->load_1();
	}

	void TangoHKLAdapter::load_1(void)
	{
		unsigned long nb_properties;

		// Get the Crystal Attributes properties.
		Tango::DbData properties;
		properties.push_back(Tango::DbDatum("Crystal"));
		_device->get_db_device()->get_attribute_property(properties);

		// the first one is the number of properties
		properties[0] >> nb_properties;

		if (nb_properties > 1) {
			unsigned long i, j;
			HklGeometry *geometry;
			HklDetector *detector;

			geometry = hkl_geometry_new_copy(_diffractometer->geometry_r);
			detector = hkl_detector_new_copy(_diffractometer->detector);

			hkl_sample_list_clear(_diffractometer->samples);
			for(i=1; i<=nb_properties; ++i) {
				// skip the _ver property
				if(!strcasecmp("_ver", properties[i].name.c_str()))
					continue;

				HklSample * sample;

				// The name of the property name is the name of a crystal.
				sample = hkl_sample_new(properties[i].name.c_str(), HKL_SAMPLE_TYPE_MONOCRYSTAL);

				// Extract the lines store in the property
				std::vector<std::string> lines;
				properties[i] >> lines;

				for(j=0; j<lines.size(); j++) {
					char *line = strdup(lines[j].c_str());
					char *last;
					char *key = strtok_r(line, "=", &last);

					if (!strcmp(key,"lattice")) {
						double a = atof(strtok_r(NULL, ";", &last));
						double b = atof(strtok_r(NULL, ";", &last));
						double c = atof(strtok_r(NULL, ";", &last));
						double alpha = atof(strtok_r(NULL, ";", &last)) * HKL_DEGTORAD;
						double beta = atof(strtok_r(NULL, ";", &last)) * HKL_DEGTORAD;
						double gamma = atof(strtok_r(NULL, ";", &last)) * HKL_DEGTORAD;
						hkl_sample_set_lattice(sample, a, b, c, alpha, beta, gamma);

						sample->lattice->a->fit = atoi(strtok_r(NULL, ";", &last));
						sample->lattice->b->fit = atoi(strtok_r(NULL, ";", &last));
						sample->lattice->c->fit = atoi(strtok_r(NULL, ";", &last));
						sample->lattice->alpha->fit = atoi(strtok_r(NULL, ";", &last));
						sample->lattice->beta->fit = atoi(strtok_r(NULL, ";", &last));
						sample->lattice->gamma->fit = atoi(strtok_r(NULL, ";", &last));
					} else if (!strcmp(key, "uxuyuz")){
						double ux = atof(strtok_r(NULL, ";", &last)) * HKL_DEGTORAD;
						double uy = atof(strtok_r(NULL, ";", &last)) * HKL_DEGTORAD;
						double uz = atof(strtok_r(NULL, ";", &last)) * HKL_DEGTORAD;
						hkl_sample_set_U_from_euler(sample, ux, uy, uz);
					} else if (!strcmp(key,"reflection")) {
						unsigned int idx = 0;
						HklSampleReflection *reflection;

						double wavelength = atof(strtok_r(NULL, ";", &last));
						double h  = atof(strtok_r(NULL, ";", &last));
						double k  = atof(strtok_r(NULL, ";", &last));
						double l  = atof(strtok_r(NULL, ";", &last));
						int flag  = atoi(strtok_r(NULL, ";", &last));

						// first set the geometry axes
						while(key = strtok_r(NULL, ";", &last))
							hkl_axis_set_value_unit(&geometry->axes[idx++], atof(key));
						geometry->source.wave_length = wavelength;

						reflection = hkl_sample_add_reflection(sample, geometry, detector, h, k, l);
						reflection->flag = flag;
					}
					free(line);
					// End of key research
				}// End for each parameters
				hkl_sample_list_append(_diffractometer->samples, sample);
			}// End for each property

			hkl_detector_free(detector);
			hkl_geometry_free(geometry);

			//_device->refresh_crystal_parameters();
		}
	}

	void TangoHKLAdapter::save(void)
	{
		omni_mutex_lock lock(_lock);

		size_t i, j, k;
		size_t len;
		Tango::DbData crystal_prop;
		Tango::DbData data_put;

		// Step 1 : clean all properties
		// FP Le mieux serait sans doute de ne pas effacer les propri�t�s d'attribut
		// avant d'avoir cr�e correctement un data_put.
		crystal_prop.push_back(Tango::DbDatum("Crystal"));
		_device->get_db_device()->get_attribute_property(crystal_prop);
		long number_of_prop = 0;
		crystal_prop[0] >> number_of_prop ;
		if( number_of_prop > 0)
			_device->get_db_device()->delete_attribute_property(crystal_prop);

		// Step 2 : create the Crystal properties
		Tango::DbDatum properties("Crystal");
		// Put number of properties (= nb of samples + 1)
		len = hkl_sample_list_len(_diffractometer->samples);
		properties << (long)(len + 1);
		data_put.push_back(properties);

		// first property is the format version
		Tango::DbDatum version("_ver");
		version << (long)FORMAT_VERSION;
		data_put.push_back(version);

		// now each sample
		for(k=0; k<len; ++k){
			HklSample *sample;
			std::vector<std::string> lines;
			char line[256];

			// the lattices values
			sample = hkl_sample_list_get_ith(_diffractometer->samples, k);
			double a       = hkl_parameter_get_value_unit(sample->lattice->a);
			double b       = hkl_parameter_get_value_unit(sample->lattice->b);
			double c       = hkl_parameter_get_value_unit(sample->lattice->c);
			double alpha   = hkl_parameter_get_value_unit(sample->lattice->alpha);
			double beta    = hkl_parameter_get_value_unit(sample->lattice->beta);
			double gamma   = hkl_parameter_get_value_unit(sample->lattice->gamma);
			// the fit flag
			int a_fit     = sample->lattice->a->fit;
			int b_fit     = sample->lattice->b->fit;
			int c_fit     = sample->lattice->c->fit;
			int alpha_fit = sample->lattice->alpha->fit;
			int beta_fit  = sample->lattice->beta->fit;
			int gamma_fit = sample->lattice->gamma->fit;

			snprintf(line, 255, "lattice=%f;%f;%f;%f;%f;%f;%d;%d;%d;%d;%d;%d",
				 a, b, c, alpha, beta, gamma,
				 a_fit, b_fit, c_fit, alpha_fit, beta_fit, gamma_fit);
			lines.push_back(line);

			// the UxUyUz parameters
			double ux, uy, uz;
			hkl_matrix_to_euler(&sample->U, &ux, &uy, &uz);
			ux *= HKL_RADTODEG;
			uy *= HKL_RADTODEG;
			uz *= HKL_RADTODEG;

			snprintf(line, 255, "uxuyuz=%f;%f;%f", ux, uy, uz);
			lines.push_back(line);
	
			// the reflections
			for(i=0; i<HKL_LIST_LEN(sample->reflections); ++i) {
				HklSampleReflection *reflection;

				reflection = hkl_sample_get_ith_reflection(sample, i);
				double wavelength = reflection->geometry->source.wave_length;
				double h = reflection->hkl.data[0];
				double k = reflection->hkl.data[1];
				double l = reflection->hkl.data[2];
				int flag = reflection->flag;

				snprintf(line, 255, "reflection=%f;%f;%f;%f;%d",
					 wavelength, h, k, l, flag);
				// Extract values of each axes
				for(j=0; j<HKL_LIST_LEN(reflection->geometry->axes); ++j) {
					char pos[256];
					double rad = hkl_axis_get_value_unit(&reflection->geometry->axes[j]);
					snprintf(pos, 255, ";%f", rad);
					strncat(line, pos, 255);
				}
				lines.push_back(line);
			}

			// Try to create property
			// Get crystal name
			Tango::DbDatum property(sample->name);
			property << lines;
			data_put.push_back(property);
		}

		//update database for this property
		_device->get_db_device()->put_attribute_property(data_put);
	}

	/***************/
	/* sample part */
	/***************/

	char const *TangoHKLAdapter::get_sample_name(void)
	{
		omni_mutex_lock lock(_lock);

		HklSample *sample = _diffractometer->samples->current;
		if(sample)
			return sample->name;
		else
			return "";
	}

	void TangoHKLAdapter::set_current_sample(char const * name)
	{
		omni_mutex_lock lock(_lock);

		HklSample *last;

		last = _diffractometer->samples->current;
		if (HKL_SUCCESS == hkl_sample_list_select_current(_diffractometer->samples, name))
			if (last != _diffractometer->samples->current){
				this->update_pseudo_axis_engines();
				this->update_ub();
				this->update_ux_uy_uz();
				this->update_reflections_angles();
				this->update_reflections();
				this->update_state_and_status();
			}
	}

	void TangoHKLAdapter::get_sample_lattices(double *a, double *b, double *c,
						  double *alpha, double *beta, double *gamma,
						  double *a_star, double *b_star, double *c_star,
						  double *alpha_star, double *beta_star, double *gamma_star)
	{
		omni_mutex_lock lock(_lock);

		HklSample * sample = _diffractometer->samples->current;
		if (!sample)
			return;

		HklLattice const *lattice = sample->lattice;
		HklLattice *reciprocal = hkl_lattice_new_copy(lattice);

		hkl_lattice_reciprocal(lattice, reciprocal);

		// direct space
		*a = hkl_parameter_get_value_unit(lattice->a);
		*b = hkl_parameter_get_value_unit(lattice->b);
		*c = hkl_parameter_get_value_unit(lattice->c);
		*alpha = hkl_parameter_get_value_unit(lattice->alpha);
		*beta = hkl_parameter_get_value_unit(lattice->beta);
		*gamma = hkl_parameter_get_value_unit(lattice->gamma);

		// reciprocal space
		*a_star = hkl_parameter_get_value_unit(reciprocal->a);
		*b_star = hkl_parameter_get_value_unit(reciprocal->b);
		*c_star = hkl_parameter_get_value_unit(reciprocal->c);
		*alpha_star = hkl_parameter_get_value_unit(reciprocal->alpha);
		*beta_star = hkl_parameter_get_value_unit(reciprocal->beta);
		*gamma_star = hkl_parameter_get_value_unit(reciprocal->gamma);

		hkl_lattice_free(reciprocal);
	}

	void TangoHKLAdapter::get_sample_fit(bool *afit, bool *bfit, bool *cfit,
					     bool *alphafit, bool *betafit, bool *gammafit,
					     bool *uxfit, bool *uyfit, bool *uzfit)
	{
		omni_mutex_lock lock(_lock);

		HklSample * sample = _diffractometer->samples->current;
		if (!sample)
			return;

		HklLattice const *lattice = sample->lattice;

		*afit = lattice->a->fit;
		*bfit = lattice->b->fit;
		*cfit = lattice->c->fit;
		*alphafit = lattice->alpha->fit;
		*betafit = lattice->beta->fit;
		*gammafit = lattice->gamma->fit;
		*uxfit = _uxfit;
		*uyfit = _uyfit;
		*uzfit = _uzfit;
	}


	void TangoHKLAdapter::set_sample_Ux(double ux)
	{
		HklSample *sample;

		sample = _diffractometer->samples->current;
		if (sample){
			if (HKL_SUCCESS == hkl_sample_set_U_from_euler(sample,
								       ux * HKL_DEGTORAD,
								       _uy * HKL_DEGTORAD,
								       _uz * HKL_DEGTORAD)){
				_ux = ux;
				this->update_ub();
			}else
				TANGO_EXCEPTION_THROW_WITHOUT_LOG("Can not set this Ux value",
								  "Set a correct value");
		}	
	}

	void TangoHKLAdapter::set_sample_Uy(double uy)
	{
		HklSample *sample;

		sample = _diffractometer->samples->current;
		if (sample){
			if (HKL_SUCCESS == hkl_sample_set_U_from_euler(sample,
								       _ux * HKL_DEGTORAD,
								       uy * HKL_DEGTORAD,
								       _uz * HKL_DEGTORAD)){
				_uy = uy;
				this->update_ub();
			}else
				TANGO_EXCEPTION_THROW_WITHOUT_LOG("Can not set this Uy value",
								  "Set a correct value");
		}
	}

	void TangoHKLAdapter::set_sample_Uz(double uz)
	{
		HklSample *sample;

		sample = _diffractometer->samples->current;
		if (sample){
			if (HKL_SUCCESS == hkl_sample_set_U_from_euler(sample,
								       _ux * HKL_DEGTORAD,
								       _uy * HKL_DEGTORAD,
								       uz * HKL_DEGTORAD)){
				_uz = uz;
				this->update_ub();
			}else
				TANGO_EXCEPTION_THROW_WITHOUT_LOG("Can not set this Uz value",
								  "Set a correct value");
		}
	}

	void TangoHKLAdapter::set_sample_AFit(bool fit)
	{
		HklSample *sample;

		sample = _diffractometer->samples->current;
		if (sample)
			sample->lattice->a->fit = fit;
	}

	void TangoHKLAdapter::set_sample_BFit(bool fit)
	{
		HklSample *sample;

		sample = _diffractometer->samples->current;
		if (sample)
			sample->lattice->b->fit = fit;
	}

	void TangoHKLAdapter::set_sample_CFit(bool fit)
	{
		HklSample *sample;

		sample = _diffractometer->samples->current;
		if (sample)
			sample->lattice->c->fit = fit;
	}

	void TangoHKLAdapter::set_sample_AlphaFit(bool fit)
	{
		HklSample *sample;

		sample = _diffractometer->samples->current;
		if (sample)
			sample->lattice->alpha->fit = fit;
	}

	void TangoHKLAdapter::set_sample_BetaFit(bool fit)
	{
		HklSample *sample;

		sample = _diffractometer->samples->current;
		if (sample)
			sample->lattice->beta->fit = fit;
	}

	void TangoHKLAdapter::set_sample_GammaFit(bool fit)
	{
		HklSample *sample;

		sample = _diffractometer->samples->current;
		if (sample)
			sample->lattice->gamma->fit = fit;
	}

	void TangoHKLAdapter::set_sample_UxFit(bool fit)
	{
		HklSample *sample;

		sample = _diffractometer->samples->current;
		if (sample)
			_uxfit = _uyfit = _uzfit = fit;
	}

	void TangoHKLAdapter::set_sample_UyFit(bool fit)
	{
		HklSample *sample;

		sample = _diffractometer->samples->current;
		if (sample)
			_uxfit = _uyfit = _uzfit = fit;
	}

	void TangoHKLAdapter::set_sample_UzFit(bool fit)
	{
		HklSample *sample;

		sample = _diffractometer->samples->current;
		if (sample)
			_uxfit = _uyfit = _uzfit = fit;
	}

	void TangoHKLAdapter::add_new_sample(std::string const & name)
	{
		omni_mutex_lock lock(_lock);

		HklSample *sample = hkl_sample_new(name.c_str(), HKL_SAMPLE_TYPE_MONOCRYSTAL);

		if (!hkl_sample_list_append(_diffractometer->samples, sample)){
			hkl_sample_free(sample);
			TANGO_EXCEPTION_THROW_WITHOUT_LOG("Can not add a sample with this name",
							  "A sample with the same name is already present in the sample list");
		}
	}

	void TangoHKLAdapter::copy_sample_as(Tango::DevString copy_name)
	{
		omni_mutex_lock lock(_lock);

		HklSample *sample;
		HklSample const *current;

		current = _diffractometer->samples->current;
		if(!current)
			TANGO_EXCEPTION_THROW_WITHOUT_LOG("No current sample set",
							  "Please set a current sample");

		sample = hkl_sample_list_get_by_name(_diffractometer->samples, copy_name);
		if (sample)
			TANGO_EXCEPTION_THROW_WITHOUT_LOG("Can not add a sample with this name",
							  "A sample with the same name is already present in the sample list");

		sample = hkl_sample_new_copy(current);
		if(sample){
			hkl_sample_set_name(sample, copy_name);
			hkl_sample_list_append(_diffractometer->samples, sample);
		}
	}

	void TangoHKLAdapter::del_sample(void)
	{
		omni_mutex_lock lock(_lock);

		HklSampleList *samples = _diffractometer->samples;
		hkl_sample_list_del(samples, samples->current);

		// add a default sample if no more sample in the list
		if(hkl_sample_list_len(samples) == 0){
			HklSample *sample = hkl_sample_new("default", HKL_SAMPLE_TYPE_MONOCRYSTAL);
			samples->current = hkl_sample_list_append(samples, sample);
		}else
			samples->current = hkl_sample_list_get_ith(samples, 0);
		this->update_ub();
		this->update_ux_uy_uz();
		this->update_reflections_angles();
		this->update_reflections();
	}

	void TangoHKLAdapter::set_lattice(const Tango::DevVarDoubleArray *argin)
	{
		omni_mutex_lock lock(_lock);

		if (argin && argin->length() != 6)
			TANGO_EXCEPTION_THROW_WITHOUT_LOG("DATA_OUT_OF_RANGE",
							  "Did not receive the exact amount of crystal parameters: A, B, C, alpha, beta, gamma");
		
		HklSample *sample = _diffractometer->samples->current;
		if (HKL_FAIL == hkl_sample_set_lattice(sample,
						       (*argin)[0],(*argin)[1], (*argin)[2],
						       (*argin)[3] * HKL_DEGTORAD,
						       (*argin)[4] * HKL_DEGTORAD,
						       (*argin)[5] * HKL_DEGTORAD))
			
			TANGO_EXCEPTION_THROW_WITHOUT_LOG("Can not set this lattice combination.",
							  "Please set a good combination");
		this->update_ub();
		this->update_reflections_angles();
	}
	
	void TangoHKLAdapter::add_reflection(const Tango::DevVarDoubleArray *argin)
	{
		omni_mutex_lock lock(_lock);

		HklSample *sample = _diffractometer->samples->current;

		if(sample){
			double h, k, l;
			HklSampleReflection *ref;

			if(argin && argin->length() == 3){
				h = (*argin)[0];
				k = (*argin)[1];
				l = (*argin)[2];
			}else{
				HklPseudoAxisEngine *engine;

				engine = hkl_pseudo_axis_engine_list_get_by_name(_diffractometer->engines_r, "hkl");

				h = hkl_parameter_get_value_unit((HklParameter *)engine->pseudoAxes[0]);
				k = hkl_parameter_get_value_unit((HklParameter *)engine->pseudoAxes[1]);
				l = hkl_parameter_get_value_unit((HklParameter *)engine->pseudoAxes[2]);
			}

			ref = hkl_sample_add_reflection(sample,
							_diffractometer->geometry_r,
							_diffractometer->detector,
							h, k, l);
			if(ref){
				this->update_reflections_angles();
				this->update_reflections();
			}
		}
	}

	void TangoHKLAdapter::del_reflection(Tango::DevShort argin)
	{
		omni_mutex_lock lock(_lock);

		if (HKL_FAIL == hkl_sample_del_reflection(_diffractometer->samples->current, argin))
			TANGO_EXCEPTION_THROW_WITHOUT_LOG("index out of range",
							  "change the reflection index");
		this->update_reflections_angles();
		this->update_reflections();
	}

	void TangoHKLAdapter::set_reflections(Matrix<double> const & img)
	{
		omni_mutex_lock lock(_lock);

		HklSample *sample = _diffractometer->samples->current;
		if (sample
		    && img.xdim == _reflections.xdim
		    && img.ydim == _reflections.ydim) {

			size_t i = 0;
			size_t j = 0;
			for(i=0; i<HKL_LIST_LEN(sample->reflections); ++i) {
				HklSampleReflection *r;

				r = hkl_sample_get_ith_reflection(sample, i);
				if (r) {
					hkl_sample_reflection_set_hkl(r, img.data[j+1], img.data[j+2], img.data[j+3]);
					hkl_sample_reflection_set_flag(r, (int)img.data[j+5]);
					if(!_device->protectReflectionAxes){
						HklAxis *axes;
						size_t len;
						size_t k;

						len = HKL_LIST_LEN(r->geometry->axes);
						axes = &r->geometry->axes[0];

						for(k=6; k<6+len; ++k)
							hkl_axis_set_value_unit(axes++, img.data[j+k]);

						hkl_geometry_update(r->geometry);
						hkl_sample_reflection_set_geometry(r, r->geometry);
					}
				}
				j += _reflections.xdim;
			}
			this->update_reflections();	
			this->update_reflections_angles();		
		}
	}

	double TangoHKLAdapter::affine_sample(std::string name)
	{
		omni_mutex_lock lock(_lock);

		HklSample *sample;
		double res = 0.;

		sample = hkl_sample_list_get_by_name(_diffractometer->samples, name.c_str());
		if(sample){
			HklSample *tmp;

			// check if the affine sample is already in the HklSampleList
			std::string name = sample->name;
			name += "(affine)";
			tmp = hkl_sample_list_get_by_name(_diffractometer->samples, name.c_str());
			hkl_sample_list_del(_diffractometer->samples, tmp);

			tmp = hkl_sample_new_copy(sample);
			hkl_sample_set_name(tmp, name.c_str());
			res = hkl_sample_affine(tmp);

			hkl_sample_list_append(_diffractometer->samples, tmp);
			_diffractometer->samples->current = tmp;

			this->update_ub();
			this->update_ux_uy_uz();
			this->update_reflections_angles();
		}

		return res;
	}


	std::vector<std::string> TangoHKLAdapter::get_samples_names(void)
	{
		omni_mutex_lock lock(_lock);

		std::vector<std::string> names;
		size_t i, len;
		HklSampleList *samples = _diffractometer->samples;

		len = hkl_sample_list_len(samples);
		for(i=0; i<len; ++i)
			names.push_back(hkl_sample_list_get_ith(samples, i)->name);

		return names;
	}

	// DEPRECATED
	void TangoHKLAdapter::get_sample_parameter_values(Tango::DevVarDoubleStringArray *argout)
	{
		omni_mutex_lock lock(_lock);

		HklSample *sample;
		HklParameter *parameter = NULL;
		std::string name;

		//check parameters
		if (argout->svalue.length() != 1)
			TANGO_EXCEPTION_THROW_WITHOUT_LOG("DATA_OUT_OF_RANGE",
							  "only one string = parameter name");

		sample = _diffractometer->samples->current;
		if(sample){
			//parameters OK
			name = argout->svalue[0];
			if(name == "a")
				parameter = sample->lattice->a;
			else if(name == "b")
				parameter = sample->lattice->b;
			else if(name == "c")
				parameter = sample->lattice->c;
			else if(name == "alpha")
				parameter = sample->lattice->alpha;
			else if(name == "beta")
				parameter = sample->lattice->beta;
			else if(name == "gamma")
				parameter = sample->lattice->gamma;

			if (parameter){
				argout->dvalue[0] = parameter->range.min;
				argout->dvalue[1] = parameter->range.max;
				argout->dvalue[2] = parameter->fit;
			}else
				TANGO_EXCEPTION_THROW_WITHOUT_LOG("Wrong parameter name",
								  "Select: a, b, c, alpha, beta, gamma");
		}
	}

	//DEPRECATED
	void TangoHKLAdapter::set_sample_parameter_values(Tango::DevVarDoubleStringArray const *argin)
	{
		omni_mutex_lock lock(_lock);

		HklSample *sample;
		HklParameter *parameter = NULL;
		std::string name;

		// check parameters
		if(argin->dvalue.length() != 3)
			TANGO_EXCEPTION_THROW_WITHOUT_LOG("DATA_OUT_OF_RANGE",
							  "set_crystal_parameter_values did not receive the right amount of scalar parameters: min, max, flag");
		if((argin->svalue.length() ) != 1)
			TANGO_EXCEPTION_THROW_WITHOUT_LOG("DATA_OUT_OF_RANGE",
							  "set_crystal_parameter_values did not receive the right amount of string parameters: parameter name");
	
		// parameters OK
		sample = _diffractometer->samples->current;
		if(!sample)
			TANGO_EXCEPTION_THROW_WITHOUT_LOG("No current sample set",
							  "Please set a current sample");

		name = argin->svalue[0];
		if(name == "a")
			parameter = sample->lattice->a;
		else if(name == "b")
			parameter = sample->lattice->b;
		else if(name == "c")
			parameter = sample->lattice->c;
		else if(name == "alpha")
			parameter = sample->lattice->alpha;
		else if(name == "beta")
			parameter = sample->lattice->beta;
		else if(name == "gamma")
			parameter = sample->lattice->gamma;

		if (parameter){
			parameter->range.min = argin->dvalue[0];
			parameter->range.max = argin->dvalue[1];
			parameter->fit = argin->dvalue[2] != 0.;
		}else
			TANGO_EXCEPTION_THROW_WITHOUT_LOG("Wrong parameter name",
							  "Select: a, b, c, alpha, beta, gamma");
		this->update_ub();
		this->update_ux_uy_uz();
		this->update_reflections_angles();
	}

	void TangoHKLAdapter::compute_u(const Tango::DevVarLongArray *argin)
	{
		omni_mutex_lock lock(_lock);

		HklSample *sample;

		// is parameter ok ?
		if (argin->length() != 2)
			TANGO_EXCEPTION_THROW_WITHOUT_LOG("Need exactly two reflections indexes",
							  "use the right number of parameters");
		
		sample = _diffractometer->samples->current;
		if (!sample)
			TANGO_EXCEPTION_THROW_WITHOUT_LOG("Can not compute the U matrix without current sample set.",
							  "Set a current sample");

		if (hkl_sample_compute_UB_busing_levy(sample, (*argin)[0], (*argin)[1]))
			TANGO_EXCEPTION_THROW_WITHOUT_LOG("can not compute the UB matrix using thoses reflections index",
							  "Use other reflections");
		this->update_ub();
		this->update_ux_uy_uz();
	}

	/*************/
	/* Axis part */
	/*************/

	void TangoHKLAdapter::read_axis(int idx, double & read, double & write)
	{
		omni_mutex_lock lock(_lock);

		AxisAdapter & axis = _axes[idx];
		read = axis.get_read();
		write = axis.get_write();
	}

	void TangoHKLAdapter::stop_all_axis(void)
	{
#ifdef WRITE_TO_PROXY_ALLOWED
		size_t i;
		for(i=0; i<_axes.size(); ++i)
			_axes[i].stop();
#endif
	}

	void TangoHKLAdapter::write_axis(AxisAdapter & axis, double value)
	{
		omni_mutex_lock lock(_lock);

		size_t i;

		this->write_axis_i(axis, value);

		// when we write on an Axis you must let all pseudo axes be synchronized
		for(i=0; i<_pseudoAxesAdapters.size(); ++i)
			_pseudoAxesAdapters[i]->_synchronize = true;
	}

	void TangoHKLAdapter::write_axis_i(AxisAdapter & axis, double value)
	{
		axis._read = axis._write = value;
		hkl_axis_set_value_unit(axis._axis_r, value);
		hkl_axis_set_value_unit(axis._axis_w, value);
	}

	/*******************/
	/* pseudoAxis part */
	/*******************/

	std::vector<std::string> TangoHKLAdapter::pseudo_axis_get_names(void)
	{
		omni_mutex_lock lock(_lock);

		std::vector<std::string> names;
		size_t i, j;

		for(i=0; i<HKL_LIST_LEN(_diffractometer->engines_r_real->engines); ++i){
			HklPseudoAxisEngine *engine;

			engine = _diffractometer->engines_r_real->engines[i];
			for(j=0; j<HKL_LIST_LEN(engine->pseudoAxes); ++j)
				names.push_back(((HklParameter *)(&engine->pseudoAxes[j]))->name);
		}

		return names;
	}

	PseudoAxisAdapter *TangoHKLAdapter::pseudo_axis_buffer_new(char const *name)
	{
		omni_mutex_lock lock(_lock);

		PseudoAxisAdapter * buffer = NULL;
		HklPseudoAxis *pseudo_r;
		HklPseudoAxis *pseudo_w;

		pseudo_r = hkl_pseudo_axis_engine_list_get_pseudo_axis_by_name(
			_diffractometer->engines_r_real, name);
		pseudo_w = hkl_pseudo_axis_engine_list_get_pseudo_axis_by_name(
			_diffractometer->engines_w_real, name);

		if(pseudo_r && pseudo_w) {
			try
			{
				buffer = new PseudoAxisAdapter(*this, pseudo_r, pseudo_w);
				_pseudoAxisAdapters.push_back(buffer);
			}
			catch (Tango::DevFailed)
			{
				delete buffer;
				buffer = NULL;
			}
		}
		return buffer;
	}

	void TangoHKLAdapter::pseudo_axis_set_initialized(PseudoAxisAdapter *buffer,
							  Tango::DevBoolean initialized)
	{
		if(initialized){
			this->update();

			omni_mutex_lock lock(_lock);

			hkl_pseudo_axis_engine_initialize(buffer->_pseudo_r->engine, NULL);
			hkl_pseudo_axis_engine_initialize(buffer->_pseudo_w->engine, NULL);
		}
	}

	void TangoHKLAdapter::pseudo_axis_set_position(PseudoAxisAdapter *buffer,
						       const Tango::DevDouble & position)
	{
		omni_mutex_lock lock(_lock);

		double value = position - buffer->_config.offset;
		int res = HKL_FAIL;

		HklPseudoAxisEngineList *engines_w_real = _diffractometer->engines_w_real;
		HklPseudoAxisEngine *engine = buffer->_pseudo_w->engine;
		HklGeometry *geometry_w_real = _diffractometer->geometry_w_real;

		hkl_parameter_set_value_unit((HklParameter *)buffer->_pseudo_w, value);
		res = hkl_pseudo_axis_engine_set(buffer->_pseudo_w->engine, NULL);
		if (HKL_SUCCESS == res){
			hkl_geometry_init_geometry(geometry_w_real,
						   engine->engines->geometries->items[0]->geometry);			
			hkl_pseudo_axis_engine_list_get(engines_w_real);

			_angles_idx = 0;
			this->update_angles();
			buffer->to_proxies();
			this->update_axis_adapters();
			this->update_pseudo_axis_adapters_from_hkl();
			this->update_pseudo_axes_adapters_from_hkl();
		}else
			TANGO_EXCEPTION_THROW_WITHOUT_LOG("Can not write on this pseudo axis",
							  "Check the sample");

	}

	PseudoAxisConfig TangoHKLAdapter::pseudo_axis_get_config(PseudoAxisAdapter *buffer)
	{
		this->update();

		omni_mutex_lock lock(_lock);

		return buffer->_config;
	}

	void TangoHKLAdapter::pseudo_axis_set_mode(PseudoAxisAdapter *buffer, Tango::DevString const & mode)
	{
		omni_mutex_lock lock(_lock);

		size_t i, len;

		len = HKL_LIST_LEN(buffer->_pseudo_r->engine->modes);
		for(i=0; i<len; ++i)
			if(!strcasecmp(mode, buffer->_pseudo_r->engine->modes[i]->name))
				break;
		if(i<len){
			hkl_pseudo_axis_engine_select_mode(buffer->_pseudo_r->engine, i);
			hkl_pseudo_axis_engine_select_mode(buffer->_pseudo_w->engine, i);
		}
	}

	void TangoHKLAdapter::pseudo_axis_get_mode_parameters(PseudoAxisAdapter *buffer, Tango::DevVarDoubleStringArray *argout)
	{
		omni_mutex_lock lock(_lock);

		HklPseudoAxisEngine *engine = buffer->_pseudo_r->engine;
		if (engine && engine->mode){
			size_t i;
			size_t len = HKL_LIST_LEN(engine->mode->parameters);

			argout->svalue.length(len);
			argout->dvalue.length(len);
			for(i=0; i<len; ++i){
				argout->svalue[i] = CORBA::string_dup(engine->mode->parameters[i].name);
				argout->dvalue[i] = hkl_parameter_get_value_unit(&engine->mode->parameters[i]);
			}
		}else
			TANGO_EXCEPTION_THROW_WITHOUT_LOG("Can not get the current Mode parameters values without a current mode set.",
							  "");
	}

	void TangoHKLAdapter::pseudo_axis_set_mode_parameters(PseudoAxisAdapter *buffer, const Tango::DevVarDoubleStringArray *argin)
	{
		omni_mutex_lock lock(_lock);

		if(argin->svalue.length() != argin->dvalue.length())
			TANGO_EXCEPTION_THROW_WITHOUT_LOG("DATA_OUT_OF_RANGE",
							  "set_mode_parameters_values did not receive the same amount between string and double values");

		HklPseudoAxisEngine *engine_r = buffer->_pseudo_r->engine;
		HklPseudoAxisEngine *engine_w = buffer->_pseudo_w->engine;
		if (engine_r && engine_w){
			size_t i;
			size_t len;

			len = argin->svalue.length();
			if (HKL_LIST_LEN(engine_r->mode->parameters) != len)
				TANGO_EXCEPTION_THROW_WITHOUT_LOG("Not the right number of parameter",
								  "gives the right number of parameters");
			
			for(i=0; i<len; ++i){
				double value = argin->dvalue[i];
				char const *name = argin->svalue[i];
				if(!strcmp(name, engine_r->mode->parameters[i].name)){
					hkl_parameter_set_value_unit(&engine_r->mode->parameters[i], value);
					hkl_parameter_set_value_unit(&engine_w->mode->parameters[i], value);
				}
			}
		}else
			TANGO_EXCEPTION_THROW_WITHOUT_LOG("Can not set the current Mode parameters values without a current mode set.",
							  "");
	}

	void TangoHKLAdapter::pseudo_axis_on(PseudoAxisAdapter *buffer)
	{
		omni_mutex_lock lock(_lock);

		for(size_t i=0; i<buffer->_axes.size(); ++i)
			buffer->_axes[i]->on();
	}

	/*******************/
	/* pseudoAxes part */
	/*******************/

	PseudoAxesAdapter *TangoHKLAdapter::pseudo_axes_adapter_get_by_name(std::string const & name)
	{
		//omni_mutex_lock lock(_lock);

		unsigned int i;
		PseudoAxesAdapter *adapter = NULL;

		for(i=0; i<_pseudoAxesAdapters.size(); ++i)
			if(_pseudoAxesAdapters[i]->get_name() == name){
				adapter = _pseudoAxesAdapters[i];
				break;
			}
		return adapter;
	}

	PseudoAxesConfig TangoHKLAdapter::pseudo_axes_get_config(size_t idx)
	{
		this->update();

		omni_mutex_lock lock(_lock);

		return _pseudoAxesAdapters[idx]->_config;
	}

	void TangoHKLAdapter::pseudo_axes_set_axis_value(size_t idx, size_t p_idx, double value)
	{
		omni_mutex_lock lock(_lock);

		size_t i;

		for(i=0; i<_pseudoAxesAdapters.size(); ++i)
			if(i == idx){
				// first unsynchronize the current PseudoAxes
				_pseudoAxesAdapters[i]->_synchronize = false;
				_pseudoAxesAdapters[i]->_config.write.data[p_idx] = value;
			} else {
				// resynchronize all other pseudoAxes
				_pseudoAxesAdapters[i]->_synchronize = true;
			}
	}

	void TangoHKLAdapter::pseudo_axes_apply(size_t idx, Matrix<double> const & write) throw (Tango::DevFailed)
	{
		omni_mutex_lock lock(_lock);

		size_t i, len;
		int res = HKL_FAIL;
		HklPseudoAxisEngineList *engines_r, *engines_w;
		HklGeometry *geometry_r, *geometry_w;

		engines_r = _diffractometer->engines_r;
		engines_w = _diffractometer->engines_w;
		len = HKL_LIST_LEN(engines_w->engines[idx]->pseudoAxes);

		geometry_r = _diffractometer->geometry_r;
		geometry_w = _diffractometer->geometry_w;

		for(i=0; i<len; ++i)
			hkl_parameter_set_value_unit((HklParameter *)(engines_w->engines[idx]->pseudoAxes[i]), write.data[i]);

		res = hkl_pseudo_axis_engine_set(engines_w->engines[idx], NULL);

		if (HKL_SUCCESS == res){
			// force auto update OFF when we write on a PseudoAxes.
			_auto_update_from_proxies = false;

			for(i=0; i<_pseudoAxesAdapters.size(); ++i)
				_pseudoAxesAdapters[i]->_synchronize = true;

			hkl_geometry_init_geometry(geometry_w,
						   engines_w->geometries->items[0]->geometry);			
			hkl_pseudo_axis_engine_list_get(engines_w);
			hkl_geometry_init_geometry(geometry_r,
						   engines_w->geometries->items[0]->geometry);			
			hkl_pseudo_axis_engine_list_get(engines_r);

			_angles_idx = 0;
			this->update_angles();
			this->update_axis_adapters();
			this->update_pseudo_axis_adapters_from_hkl();
			this->update_pseudo_axes_adapters_from_hkl();
		}else
			TANGO_EXCEPTION_THROW_WITHOUT_LOG("Can not write on this pseudo axis",
							  "Check the sample");

	}

	void TangoHKLAdapter::pseudo_axes_set_mode(size_t idx, const Tango::DevString name)
	{
		omni_mutex_lock lock(_lock);

		size_t i;
		HklPseudoAxisEngine *engine = _diffractometer->engines_r->engines[idx];

		// check if we try to set the same mode than the current one
		if (engine->mode)
			if(!strcasecmp(name, engine->mode->name))
				return;

		// no so set the mode if possible
		size_t len = HKL_LIST_LEN(engine->modes);
		for(i=0; i<len; ++i)
			if(!strcasecmp(name, engine->modes[i]->name))
				break;
		if(i<len){
			hkl_pseudo_axis_engine_select_mode(_diffractometer->engines_r->engines[idx], i);
			hkl_pseudo_axis_engine_select_mode(_diffractometer->engines_w->engines[idx], i);
			hkl_pseudo_axis_engine_select_mode(_diffractometer->engines_r_real->engines[idx], i);
			hkl_pseudo_axis_engine_select_mode(_diffractometer->engines_w_real->engines[idx], i);
			_pseudoAxesAdapters[idx]->update_axes_i(engine);
			_pseudoAxesAdapters[idx]->update();
		}
	}

	void  TangoHKLAdapter::pseudo_axes_set_mode_parameters(size_t idx, Matrix<double> const & values)
	{
		omni_mutex_lock lock(_lock);

		size_t i;
		HklPseudoAxisEngine *engine = _diffractometer->engines_r->engines[idx];
		size_t len = HKL_LIST_LEN(engine->mode->parameters);
		if(len == values.xdim)
			for(i=0; i<len; ++i){
				double & value = values.data[i];

				hkl_parameter_set_value_unit(&_diffractometer->engines_r->engines[idx]->mode->parameters[i],
							     value);
				hkl_parameter_set_value_unit(&_diffractometer->engines_w->engines[idx]->mode->parameters[i],
							     value);
				hkl_parameter_set_value_unit(&_diffractometer->engines_r_real->engines[idx]->mode->parameters[i],
							     value);
				hkl_parameter_set_value_unit(&_diffractometer->engines_w_real->engines[idx]->mode->parameters[i],
							     value);
			}
	}

	void TangoHKLAdapter::pseudo_axes_init(size_t idx)
	{
		// temporary until the CAPOEIRA applycation has been modify
		if(_diffractometer->engines_r->engines[idx]->mode->initialize){
			omni_mutex_lock lock(_lock);

			HklPseudoAxisEngine *engine_r = _diffractometer->engines_r->engines[idx];
			HklPseudoAxisEngine *engine_w = _diffractometer->engines_w->engines[idx];
			HklPseudoAxisEngine *engine_r_real = _diffractometer->engines_r_real->engines[idx];
			HklPseudoAxisEngine *engine_w_real = _diffractometer->engines_w_real->engines[idx];

			hkl_pseudo_axis_engine_initialize(engine_r, NULL);
			hkl_pseudo_axis_engine_initialize(engine_w, NULL);
			hkl_pseudo_axis_engine_initialize(engine_r_real, NULL);
			hkl_pseudo_axis_engine_initialize(engine_w_real, NULL);
		}else
			this->pseudo_axes_apply(idx, _pseudoAxesAdapters[idx]->_config.write);
	}

	void TangoHKLAdapter::pseudo_axes_add_dynamic_attributes(size_t idx)
	{
		size_t i;

		// check if all the PseudoAxes were instantiated
		for(i=0; i<_pseudoAxesAdapters.size(); ++i)
			if(!_pseudoAxesAdapters[i]->_device)
				return;

		/*
		 * we need to attach the dynamic attributes after all instance of
		 * the PseudoAxes devices were started. otherwise it cause problem.
		 * only add the attributs if they were not already added.
		 */
		for(i=0; i<_pseudoAxesAdapters.size(); ++i)
			_pseudoAxesAdapters[i]->add_dynamic_attributes_i();
	}

	void TangoHKLAdapter::pseudo_axes_remove_dynamic_attributes(size_t idx)
	{
		size_t i;		
		PseudoAxesAdapter *adapter;

		adapter = _pseudoAxesAdapters[idx];
		if(adapter->_device){
			for(i=0; i<adapter->_dynamic_attribute_pseudo_axes_axis.size(); ++i)
				adapter->_device->remove_attribute(adapter->_dynamic_attribute_pseudo_axes_axis[i], false);
			adapter->_device = NULL;
		}
	}

	void TangoHKLAdapter::pseudo_axes_create_and_start_devices(void)
	{
		omni_mutex_lock lock(_lock);

		unsigned int i, j;

		Tango::Util *tg = Tango::Util::instance();
		Tango::Database *db = tg->get_database();
		for(i=0; i<_pseudoAxesAdapters.size(); ++i){
			std::string dev_name = _pseudoAxesAdapters[i]->get_proxy_name();

			// first check if the device is already defined in the database
			try{
				Tango::DbDevImportInfo my_device_import_info;

				my_device_import_info = db->import_device(dev_name);
			} catch (Tango::DevFailed &) {
				Tango::DbDevInfo my_device_info;

				// add the device to the database
				my_device_info.name = dev_name.c_str();
				my_device_info._class = "PseudoAxes";
				my_device_info.server = tg->get_ds_name().c_str();

				db->add_device(my_device_info);

				// add the right properties to that device
				Tango::DbDatum DiffractometerProxy("DiffractometerProxy"), EngineName("EngineName");
				Tango::DbData properties;
				DiffractometerProxy << _device->name();
				EngineName << _pseudoAxesAdapters[i]->get_name();
				properties.push_back(DiffractometerProxy);
				properties.push_back(EngineName);
				db->put_device_property(dev_name,properties); 
			

				// now start the device
				const std::vector<Tango::DeviceClass *> *cl_list = tg->get_class_list();
				for(j=0; j<cl_list->size(); ++j){
					if((*cl_list)[j]->get_name() == "PseudoAxes"){
						try{
							Tango::DevVarStringArray na;
							na.length(1);
							na[0] = dev_name.c_str();
							(*cl_list)[j]->device_factory(&na);
							std::cout << "Started " << dev_name << std::endl;
							break;
						}
						catch (Tango::DevFailed &e)
						{
						}
					}
				}
			}
		}
	}

	void TangoHKLAdapter::pseudo_axes_set_initialized(size_t idx, bool initialized)
	{
		if(initialized){
			this->update();

			omni_mutex_lock lock(_lock);

			HklPseudoAxisEngine *engine_r = _diffractometer->engines_r->engines[idx];
			HklPseudoAxisEngine *engine_w = _diffractometer->engines_w->engines[idx];
			HklPseudoAxisEngine *engine_r_real = _diffractometer->engines_r_real->engines[idx];
			HklPseudoAxisEngine *engine_w_real = _diffractometer->engines_w_real->engines[idx];

			hkl_pseudo_axis_engine_initialize(engine_r, NULL);
			hkl_pseudo_axis_engine_initialize(engine_w, NULL);
			hkl_pseudo_axis_engine_initialize(engine_r_real, NULL);
			hkl_pseudo_axis_engine_initialize(engine_w_real, NULL);
		}
	}

	/**********************/
	/* Dynamic attributes */
	/**********************/

	void TangoHKLAdapter::create_axes_dynamic_attributes(void)
	{
		size_t i;
		size_t len = _axes.size();
		_dynamic_attribute_axes_names.resize(len, 1);
		for(i=0; i<len; ++i){
			std::string name;
			AxisAttrib *att;
			size_t l;

			// compute the diffractometer axis names
			name = "Axis";
			l = name.size();
			name += _axes[i].get_name();
			name[l] = toupper(name[l]);

			// create the AxisAttrib to deal with the proxy connection.
			att = new AxisAttrib(name.c_str(), _axes[i]);
			_dynamic_attribute_axes.push_back(att);
			_dynamic_attribute_axes_names.data[i] = const_cast<char *>(att->get_name().c_str());
		}
	}

	void TangoHKLAdapter::destroy_axes_dynamic_attributes(void)
	{
		size_t i;
		size_t len = _dynamic_attribute_axes.size();

		for(i=0; i<len; ++i)
			delete _dynamic_attribute_axes[i];
	}

	void TangoHKLAdapter::attach_dynamic_attributes_to_device(void)
	{
		size_t i;

		for(i=0; i<_dynamic_attribute_axes.size(); ++i)
			_device->add_attribute(_dynamic_attribute_axes[i]);

	}

	void TangoHKLAdapter::detach_dynamic_attributes_from_device(void)
	{
		size_t i;

		for(i=0; i<_dynamic_attribute_axes.size(); ++i)
			_device->remove_attribute(_dynamic_attribute_axes[i], false);

	}

	bool TangoHKLAdapter::get_auto_update_from_proxies(void)
	{
		omni_mutex_lock lock(_lock);

		return _auto_update_from_proxies;
	}

	void TangoHKLAdapter::set_auto_update_from_proxies(bool update)
	{
		bool old = _auto_update_from_proxies;
		_auto_update_from_proxies = update;
		if(old = false && update == true)
			this->update();
	}

}	//	namespace

std::string hkl_axes_consign_as_string(HklAxis const **axes, size_t axes_len)
{
	size_t i;
	std::string out;

	std::ostringstream line, tmp;
	for(i=0; i<axes_len; ++i) {
		HklParameter const *axis = (HklParameter const *)axes[i];
		double value = hkl_parameter_get_value_unit(axis);

		out += "    \"";
		out += axis->name;
		out + "\" ";
		std::ostringstream tmp;
		tmp << showpos;
		tmp << value << "�";
		out += tmp.str();
	}

	return out;
}
