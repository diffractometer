#include "TangoHKLAdapterFactory.h"
#include "TangoHKLAdapter.h"
#include "Diffractometer.h"

namespace Diffractometer_ns
{
	/**
	 *
	 * Static instance of the factory

	 */
	TangoHKLAdapterFactory * TangoHKLAdapterFactory::_instance = NULL;

	/**
	 *
	 * Return the instance of the factory
	 *
	 */
	TangoHKLAdapterFactory * TangoHKLAdapterFactory::instance(void)
	{
		if (!_instance)
			_instance = new TangoHKLAdapterFactory();
		return _instance;
	}

	/**
	 *	method:	DiffractometerFactory::createDiffractometer
	 *
	 *	description: create a hkl library associated with device name
	 *
	 * @return	an instance of hkl adapter which contains an instance of the library
	 *
	 */
	TangoHKLAdapter * TangoHKLAdapterFactory::attach_diffractometer_device(
		Diffractometer *device,
		HklGeometryType type)
	{
		omni_mutex_lock lock(_lock);

		TangoHKLAdapter * adapter = NULL;

		// Check if it already build
		adapterMap_t::const_iterator iter;
		iter = _adapterMap.find( device );

		if (iter == _adapterMap.end()) {
			adapter = new TangoHKLAdapter(device, type);
			_adapterMap[device] = adapter;
			std::cout <<  "TangoHKLAdapterFactory::attach_device new Diffractometer device " << device->get_name() << " created." << std::endl;
		} else
			adapter = iter->second;

		return adapter;
	}

	void TangoHKLAdapterFactory::detach_diffractometer_device(Diffractometer *device)
	{
		omni_mutex_lock lock(_lock);

		// Find the name in map
		adapterMap_t::iterator iter;
		iter = _adapterMap.find( device );

		if (iter != _adapterMap.end()) {
			// Do Delete it as it is used by the PseudoAxes.
			//delete iter->second;
			//_adapterMap.erase(iter);
			std::cout <<  "TangoHKLAdapterFactory::removeDiffractometer Diffractometer device " << device->get_name() << " removed." << std::endl;
		}
	}

	/**
	 * @brief Get the TangoHKLAdapter associated to a device named:
	 * @param devicename The name of the device.
	 * @return The TangoHKLAdapter pointer of the Device or NULL if the device is not present.
	 */
	TangoHKLAdapter * TangoHKLAdapterFactory::get_diffractometer_adapter(std::string const & diffractometerProxyName)
	{
		omni_mutex_lock lock(_lock);

		TangoHKLAdapter * adapter = NULL;

		adapterMap_t::iterator iter = _adapterMap.begin();
		adapterMap_t::iterator end = _adapterMap.end();
		while(iter != end) {
			if (iter->first->get_name() == diffractometerProxyName) {
				adapter = iter->second;
				break;
			}
			++iter;
		}

		return adapter;
	}

	TangoHKLAdapterFactory::~TangoHKLAdapterFactory(void)
	{
		if (_instance) {
			// remove all undeleted devices.
			adapterMap_t::iterator iter = _adapterMap.begin();
			adapterMap_t::iterator end = _adapterMap.end();
			while(iter != end) {
				delete iter->second;
				++iter;
			}
			_adapterMap.clear();
			_instance = NULL;
		}
	}

}
