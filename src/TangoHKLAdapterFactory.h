#ifndef _TANGOHKLADAPTERFACTORY_H_
#define _TANGOHKLADAPTERFACTORY_H_

#include <map>
#include <string>
#include <hkl/hkl-geometry-factory.h>
#include <omnithread.h>

// forward declaration
namespace Diffractometer_ns
{ 
	class Diffractometer;
	class TangoHKLAdapter;
}

namespace Diffractometer_ns
{
	typedef std::map<Diffractometer*, TangoHKLAdapter*> adapterMap_t;

	/**
	 * Class which create an association between HKL Library, Diffractometer and Pseudo Axis
	 */
	class TangoHKLAdapterFactory
	{
		public :
			static TangoHKLAdapterFactory *instance(void);

			// register/unregister Diffractometer device
			TangoHKLAdapter * attach_diffractometer_device(Diffractometer *device, HklGeometryType type);

			void detach_diffractometer_device(Diffractometer *device);

			TangoHKLAdapter *get_diffractometer_adapter(std::string const & diffractometerProxyName);

		protected :

			// Constructor
			TangoHKLAdapterFactory() {};

			// Destructor
			virtual ~TangoHKLAdapterFactory(void);

		private :
			omni_mutex _lock;
			adapterMap_t _adapterMap;

			static TangoHKLAdapterFactory *_instance;
	};
}

#endif //_TANGOHKLADAPTERFACTORY_H_
