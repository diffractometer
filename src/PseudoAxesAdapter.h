#ifndef __PSEUDOAXES_ADAPTER_H__
#define __PSEUDOAXES_ADAPTER_H__

#include "macros.h"
#include "AxisAdapter.h"
#include "Matrix.h"

// forward declaration
namespace PseudoAxes_ns {
	class PseudoAxes;
}

namespace Diffractometer_ns {

	//forwar declaration
	class TangoHKLAdapter;

	struct PseudoAxesConfig {
		Tango::DevState state;
		std::string status;
		Tango::DevBoolean initialized;
		Tango::DevString mode;
		Matrix<double> parameters;
		Matrix<char *> parameters_names;
		Matrix<char *> pseudo_axes_names;
		Matrix<char *> mode_names;
		Matrix<double> read;
		Matrix<double> write;
	};

	class PseudoAxesAdapter {
		friend class TangoHKLAdapter;

	public:
		PseudoAxesAdapter(TangoHKLAdapter *hklAdapter, size_t _idx,
				  HklPseudoAxisEngine *engine_r,
				  HklPseudoAxisEngine *engine_w);

		virtual ~PseudoAxesAdapter(void);

		PseudoAxesConfig get_config(void);

		std::string const & get_proxy_name(void) const {return _proxy_name;}
		char const * get_name(void) const;

		void set_axis_value(size_t idx, double value);
		void set_mode(const Tango::DevString name);
		void set_initialized(const Tango::DevBoolean initialized);
		void set_parameters(const Matrix<double> values);

		void set_device(PseudoAxes_ns::PseudoAxes *device) {_device = device;}

		void init(void);
		void load(void);
		void save(void);
		void apply(void);

		void add_dynamic_attributes(void);
		void remove_dynamic_attributes(void);

	private:
		TangoHKLAdapter *_hklAdapter;
		HklPseudoAxisEngine *_engine_r;
		HklPseudoAxisEngine *_engine_w;
		size_t _idx;
		bool _synchronize;
		PseudoAxes_ns::PseudoAxes *_device;
		PseudoAxesConfig _config;
		std::string _proxy_name;
		std::vector<Tango::Attr*> _dynamic_attribute_pseudo_axes_axis;
		std::vector<AxisAdapter *> _axes;

		void update_axes_i(HklPseudoAxisEngine *engine);
		void update(void);
		void load_1(void);
		void add_dynamic_attributes_i(void);
	};
}


#endif // __PSEUDOAXES_ADAPTER_H__
