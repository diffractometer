#! /usr/bin/env python
# encoding: utf-8
# Thomas Nagy, 2006-2008 (ita)

import Options

# the following two variables are used by the target "waf dist"
VERSION='3.1.0'
APPNAME='ds_Diffractometer'

# these variables are mandatory ('/' are converted automatically)
srcdir = '.'
blddir = 'build'

def set_options(opt):
	opt.tool_options('compiler_cxx')
        opt.add_option('--soleil', action='store_true', default=False, help='Build for the Soleil site')

def configure(conf):
	conf.check_tool('compiler_cxx')
	conf.add_os_flags('LIBPATH_TANGO')
	conf.add_os_flags('CPPPATH_TANGO')
	if Options.options.soleil:
		conf.env['LIB_HKL'] = ['hkl', 'GSL', 'GSLcblas']
		#conf.env['LIBPATH_HKL'] = '${SOLEIL_ROOT}/sw-support/HKL/lib'
		#conf.env['INCPATH_HKL'] = '${SOLEIL_ROOT}/sw-support/HKL/include'
		prefix = '${HOME}/projects/'
		conf.env['LIBPATH_HKL'] = [prefix + 'hkl/build/default/src',
						'${SOLEIL_ROOT}/sw-support/GSL/lib']
		conf.env['CPPPATH_HKL'] = [prefix + 'hkl/include',
						'${SOLEIL_ROOT}/sw-support/GSL/include']
		conf.env['RPATH_HKL'] = [prefix + 'hkl/build/default/src']
		conf.env['LIB_TANGO'] = ['log4tango', 'tango', 'COSDynamic4', 'COS4', 'omnithread', 'omniORB4']
		conf.env['LIBPATH_TANGO'] = ['${SOLEIL_ROOT}/sw-support/Tango/lib',
						 '${SOLEIL_ROOT}/sw-support/OmniORB/lib']
		conf.env['CPPPATH_TANGO'] = ['${SOLEIL_ROOT}/sw-support/Tango/include',
						 '${SOLEIL_ROOT}/sw-support/OmniORB/include']
	else:
		conf.check_cfg(atleast_pkgconfig_version='0.0.0')
		conf.check_cfg(package='hkl', args='--cflags --libs')
		
		#tango < 7.0.0 do not have a pkgconfig file
		if conf.env['LIBPATH_TANGO'] or conf.env['CPPPATH_TANGO']:
			conf.env['LIB_TANGO'] = ['log4tango', 'tango']
		else:
			conf.check_cfg(package='tango', args='--cflags --libs')

def build(bld):
	bld.add_subdirs('src')
